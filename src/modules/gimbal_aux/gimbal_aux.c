#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include <drivers/drv_hrt.h>
#include <systemlib/err.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include <uORB/uORB.h>
#include <math.h>
#include <systemlib/systemlib.h>
#include <drivers/drv_gpio.h>
#include <drivers/drv_pwm_output.h>
#include <uORB/topics/vehicle_attitude.h> /*subscribe vehicle attitude data*/
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/range_finder.h>
#include <systemlib/param/param.h>

__EXPORT int gimbal_aux_main(int argc, char *argv[]);
int gimbal_aux_thread_main(int argc, char *argv[]);

static void usage(const char *reason);

static bool thread_running = false;
static bool thread_should_exit = false;
static int daemon_task;

int 	vehicle_attitude_sub;
int		_att_sub;				/**< vehicle attitude subscription */

orb_advert_t	_actuators_2_pub;

orb_id_t _actuators_id;	/**< pointer to correct actuator controls1 uORB metadata structure */



static void usage(const char *reason)
{
    if (reason) {
        fprintf(stderr, "%s\n", reason);
    }

    fprintf(stderr, "usage: control gimbal from aux 1 and aux 2\n\n");
    exit(1);
}
int gimbal_aux_main(int argc, char *argv[])
{
    if (argc < 2) {
        usage("gimbal_aux start/usage/stop");
    }

    if (!strcmp(argv[1], "start")) {
        if (thread_running) {
            warnx("gimbal_aux already running\n");
            exit(0);
        }

        thread_should_exit = false;
        daemon_task = task_spawn_cmd("gimbal_aux",
                         SCHED_DEFAULT,
                         SCHED_PRIORITY_MAX - 5,
                         2000,
                         gimbal_aux_thread_main,
                         (argv) ? (char * const *)&argv[2] : (char * const *)NULL);
        exit(0);
    }

    if (!strcmp(argv[1], "stop")) {
        thread_should_exit = true;
        exit(0);
    }

    if (!strcmp(argv[1], "status")) {
        if (thread_running) {
            warnx("gimbal_aux running");

        } else {
            warnx("gimbal_aux stopped");
        }

        exit(0);
    }

    usage("unrecognized comlmand");
    exit(1);
}



int
gimbal_aux_thread_main(int argc, char *argv[])
{

	param_t _param_system_id = param_find("MAV_SYS_ID");
	int system_id;
	param_get(_param_system_id, &(system_id));

	struct actuator_controls_s			_actuators;			/**< actuator controls */
	memset(&_actuators, 0, sizeof(_actuators));

	struct range_finder_s range_finder;
	memset(&range_finder, 0, sizeof(range_finder));

	struct vehicle_attitude_s	_att;			/**< vehicle attitude */
	memset(&_att, 0, sizeof(_att));

	int	fd;
	unsigned servo_count = 0;
	/* subscribe to attitude topic */
	int att_sub_fd = orb_subscribe(ORB_ID(vehicle_attitude));
	int range_finder_sub = orb_subscribe(ORB_ID(range_finder));			/**< distance measurement */

	/*limit subscription rate, interval 20ms which is 50hz subscription rate*/
	orb_set_interval(att_sub_fd, 20);
	/* one could wait for multiple topics with this technique, just using one here */
	struct pollfd fds[] = {
		{ .fd = att_sub_fd,   .events = POLLIN },

	};

	int error_counter = 0;
	/*open the 6 aux pins*/
	fd = open(PX4FMU_DEVICE_PATH, O_RDWR);

	if (fd < 0)
		errx(1, "open fail");

	if (ioctl(fd, PWM_SERVO_ARM, 0) < 0)       err(1, "servo arm failed");
	/*we will get 6 servos if nothing goes wrong*/
	if (ioctl(fd, PWM_SERVO_GET_COUNT, (unsigned long)&servo_count) != 0) {
		err(1, "Unable to get servo count\n");
	}

	warnx("Testing aux 1 and aux 2");
	//printf("[gimbal_aux]: sys_id: %d\n", system_id);

	_actuators_2_pub = -1;


	/*sweeping signal for Dr.Lin*/
//	double f0 =0.01;
//	double f1 = 5;
//	double t1 = 60;
//	double t = 0;
//	double f = 0;
//	double a = 0.1;

	float increasing_pwm = 0;
	uint64_t first_pwm_time = hrt_absolute_time();

	while (!thread_should_exit) {
		/* wait for sensor update of 1 file descriptor for 1000 ms (1 second) */
		int poll_ret = poll(fds, 1, 1000);
		_actuators_id = 0;
		/* handle the poll result */
		if (poll_ret == 0) {
			/* this means none of our providers is giving us data */
			printf("[gimbal_aux] Got no data within a second\n");
		} else if (poll_ret < 0) {
			/* this is seriously bad - should be an emergency */
			if (error_counter < 10 || error_counter % 50 == 0) {
				/* use a counter to prevent flooding (and slowing us down) */
				printf("[gimbal_aux] ERROR return value from poll(): %d\n"
					, poll_ret);
			}
			error_counter++;
		} else {

			/*make actuator id valid if received attitude*/
			_actuators_id = ORB_ID(actuator_controls_2);


			if (fds[0].revents & POLLIN) {
				/* obtained data for the first file descriptor */

				/* copy sensors raw data into local buffer */
				orb_copy(ORB_ID(vehicle_attitude), att_sub_fd, &_att);


/*
				printf("[gimbal_aux] att:\t%8.4f\t%8.4f\t%8.4f\n",
					(double)_att.roll,
					(double)_att.pitch,
					(double)_att.yaw);
*/




			bool range_finder_updated;

			/*vehicle laser position*/
			orb_check(range_finder_sub, &range_finder_updated);

			if (range_finder_updated) {
				orb_copy(ORB_ID(range_finder), range_finder_sub, &range_finder);
				range_finder.distance  = range_finder.distance * cosf(_att.roll) * cosf(_att.pitch);
				//printf("gimbal_aux :range: %8.4f\n", (double)range_finder.distance);

			}

			float hokuyo_angle_compansation; /*angle in degree*/

			if (range_finder.distance <= 0.3f)
			{
			     hokuyo_angle_compansation = 5.0f;
			}

			else if (0.3f < range_finder.distance && range_finder.distance < 0.6f)
			{
			     hokuyo_angle_compansation = 6.0f * range_finder.distance + (12.0f * (float)pow((7.0f - 10.0f * range_finder.distance),0.5f))/5.0f - 8.0f/5.0f;
			}

			else if (0.6f <= range_finder.distance && range_finder.distance <= 0.9f)
			{
			     hokuyo_angle_compansation = 8.0f - 6.0f * range_finder.distance;
			}

			else if (0.9f < range_finder.distance && range_finder.distance < 1.2f)
			{
			     hokuyo_angle_compansation = 6.0f * range_finder.distance - (12.0f * (float)pow(2.0f,0.5f)*(float)pow((5.0f * range_finder.distance - 4.0f),0.5f))/5.0f - 2.0f/5.0f;
			}

			else
			{
			     hokuyo_angle_compansation = 2;
			}

			hokuyo_angle_compansation *= (float)0.01745;
			//printf("gimbal_aux: %8.4f\n", (double)hokuyo_angle_compansation);

			/*assign different values according to UAVs*/
			switch(system_id){
			//printf("enterning loop\n");

			case 1:

	 			_actuators.control[0] = -_att.roll * (float)1.1674 - (float)0.2719;
				_actuators.control[1] = -(_att.pitch - (float)hokuyo_angle_compansation) * (float)1.2185 - (float)0.565;
				//printf("gimbal_aux: using UAV1\n");
				break;

			case 2:

				_actuators.control[0] = -_att.roll * (float)1.4702 - (float)0.3136;
				_actuators.control[1] = -(_att.pitch - (float)hokuyo_angle_compansation - (float)0.0523) * (float)1.327 - (float)0.0133;
				//printf("gimbal_aux: using UAV2\n");
				break;

			case 3:

				_actuators.control[0] = -_att.roll * (float)1.1952 + (float)0.1315;
				_actuators.control[1] = -(_att.pitch - (float)hokuyo_angle_compansation) * (float)1.1626 - (float)0.388;
				//printf("gimbal_aux: using UAV3\n");
				break;

			case 4:

				_actuators.control[0] = -_att.roll * (float)1.2043 - (float)0.4456;
				_actuators.control[1] = -(_att.pitch - (float)hokuyo_angle_compansation) * (float)1.1507 - (float)0.594;
				break;

			case 5:


				_actuators.control[0] = -_att.roll * (float)1.2043 - (float)0.4456;
				_actuators.control[1] = -(_att.pitch - (float)hokuyo_angle_compansation) * (float)1.1507 - (float)0.594;
				break;

			case 6:

				//printf("gimbal_aus time passed: %8.4f %8.4f\n", (double)((int)((hrt_absolute_time() - first_pwm_time) / 1000000.0f) / 10), (double)((int)(hrt_absolute_time() - first_pwm_time) / 1000000.0f));
				if(((int)((hrt_absolute_time() - first_pwm_time) / 1000000.0f) / 6) > 7)
					first_pwm_time = hrt_absolute_time();

				increasing_pwm = -0.8  + 0.2 * ((int)((hrt_absolute_time() - first_pwm_time) / 1000000.0f) / 6);
				//printf("pwm: %8.4f\n", (double)increasing_pwm);
				_actuators.control[0] = increasing_pwm;

				break;


			}



			_actuators.control[2] = 0.0f;
			_actuators.control[3] = 0.0f;

			_actuators.timestamp = hrt_absolute_time();


					/*control the maximum output of the gimbal control*/

//					printf("roll: %8.4f\n", (double)_att.roll);
//					printf("pitch: %8.4f\n", (double)_att.pitch);


			if (_actuators_2_pub > 0) {
				/*------------------this is where the actuators start to spin--------*/
				orb_publish(_actuators_id, _actuators_2_pub, &_actuators);

			} else if (_actuators_id) {
				/*------------------this is where the actuators start to advertise-----*/
				_actuators_2_pub = orb_advertise(_actuators_id, &_actuators);

			}

		}
	}

	}

    warnx("gimbal_aux exiting");
    thread_running = false;
    close(fd);

    fflush(stdout);

    return 0;
}

