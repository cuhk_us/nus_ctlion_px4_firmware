/****************************************************************************
 *
 Created by Hailong for communication with high level processor.
 *
 ****************************************************************************/
 
/**
Hailong
 */
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <nuttx/config.h>
#include <nuttx/sched.h>
#include <uORB/uORB.h>
#include <systemlib/systemlib.h>
#include <systemlib/err.h>
#include <uORB/topics/position_serialport.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/preflightcheck.h>
#include <uORB/topics/vehicle_status.h>
#include <uORB/topics/odroid_status.h>
#include <uORB/topics/odroid_preflight_status.h>
#include <uORB/topics/mission_result.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/actuator_controls.h>

#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions

#include "serialFunc.h"


extern "C" __EXPORT int serialport_main(int argc, char *argv[]);

static bool       _mea_start=false;

static bool       _serial_start=false;

//static bool        _mea_stop=false;

static int         _handshake_state=1;

class Serialport
{
public:
	/**
	 * Constructor
	 */
	Serialport();

	/**
	 * Destructor, also kills the serialport task.
	 */
	~Serialport();

	/**
	 * Start the serialport task.
	 *
	 * @return		OK on success.
	 */
	int		start();



private:

	/*	 * Main serialport collection task.
	 */
	bool 		_task_should_exit;

//	bool        _serial_start;

	bool        _gps_valid;

	bool        _location_valid;

	bool        _take_off;

	bool        _land;

	bool        _obtain_position;


	int 		_serialport_task;

	int		    _local_pos_sub;

	int         _global_pos_sub;

	int         _prechk_sub;

	int         _vehicle_sub;

	int         _mission_result_sub;

	int        _uart_fd;

	int        _odroid_behavior;

	int        _odroid_preflight_behavior;

	int        _start_behavior;

	int        _vehicle_attitude_sub;

	int        _actuators_0_sub;



	struct position_serialport_s _serial_reference;

	struct vehicle_local_position_s _local_pos;

	struct vehicle_global_position_s _global_pos;

	struct preflightcheck_s			_prechk;

	struct vehicle_status_s  _vehicle_status;

	struct odroid_status_s _odroid_status;

	struct  odroid_preflight_status_s _odroid_preflight_status;

	struct mission_result_s _mission_result;

	struct vehicle_attitude_s _vehicle_attitude;

	struct actuator_controls_s _actuators;

	void        poll_subscriptions();

	void        measurement_poll();

	orb_advert_t 	_serial_pub;
	orb_advert_t 	_odroid_status_pub;
	orb_advert_t    _odroid_preflight_status_pub;

	int        _count;

	static void	task_main_trampoline(int argc, char *argv[]);
	void		task_main();

};


namespace serialport
{

Serialport	*g_serialport = nullptr;
}

Serialport::Serialport() :
			_task_should_exit(false),
			_gps_valid(false),
			_location_valid(false),
			_take_off(false),
			_land(false),
			_obtain_position(false),
				_serialport_task(-1),
		        _local_pos_sub(-1),
		        _global_pos_sub(-1),
		        _prechk_sub(-1),
		        _vehicle_sub(-1),
		        _mission_result_sub(-1),
		        _uart_fd(-1),
		    	_actuators_0_sub(-1),
		    	_serial_pub(-1),
		    	_odroid_status_pub(-1),
		    	_odroid_preflight_status_pub(-1),
		        _count(0)
{
	memset(&_serial_reference, 0, sizeof(_serial_reference));
	memset(&_local_pos, 0, sizeof(_local_pos));
	memset(&_global_pos, 0, sizeof(_global_pos));
	memset(&_prechk, 0 ,sizeof(_prechk));
	memset(&_odroid_status, 0, sizeof(_odroid_status));
	memset(&_odroid_preflight_status, 0, sizeof(_odroid_preflight_status));
	memset(&_mission_result, 0, sizeof(_mission_result));
	memset(&_vehicle_attitude, 0, sizeof(_vehicle_attitude));
	memset(&_actuators, 0, sizeof(_actuators));


}

Serialport::~Serialport()
{
	if (_serialport_task != -1) {

		/* task wakes up every 100ms or so at the longest */
		_task_should_exit = true;

		/* wait for a second for the task to quit at our request */
		unsigned i = 0;

		do {
			/* wait 20ms */
			usleep(20000);

			/* if we have given up, kill it */
			if (++i > 50) {
				task_delete(_serialport_task);
				break;
			}
		} while (_serialport_task != -1);
	}

	serialport::g_serialport = nullptr;
}

void
Serialport::poll_subscriptions()
{
	bool updated;

	orb_check(_local_pos_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_local_position), _local_pos_sub, &_local_pos);
	}

	orb_check(_global_pos_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_global_position), _global_pos_sub, &_global_pos);
	}

	orb_check(_prechk_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(preflightcheck), _prechk_sub, &_prechk);
	}


	orb_check(_vehicle_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_status), _vehicle_sub, &_vehicle_status);
	}

	orb_check(_mission_result_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(mission_result), _mission_result_sub, &_mission_result);
	}

	orb_check(_vehicle_attitude_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_attitude), _vehicle_attitude_sub, &_vehicle_attitude);
	}

	orb_check(_actuators_0_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(actuator_controls_0), _actuators_0_sub, &_actuators);
	}
}


void
Serialport::measurement_poll()
{
	bool updated;

	orb_check(_local_pos_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_local_position), _local_pos_sub, &_local_pos);
	}

}

void
Serialport::task_main_trampoline(int argc, char *argv[])
{
	serialport::g_serialport->task_main();
}



void
Serialport::task_main()
{

	_local_pos_sub = orb_subscribe(ORB_ID(vehicle_local_position));
	_global_pos_sub = orb_subscribe(ORB_ID(vehicle_global_position));
	_prechk_sub = orb_subscribe(ORB_ID(preflightcheck));
	_vehicle_sub = orb_subscribe(ORB_ID(vehicle_status));
	_mission_result_sub = orb_subscribe(ORB_ID(mission_result));
	_vehicle_attitude_sub=orb_subscribe(ORB_ID(vehicle_attitude));
	_actuators_0_sub=orb_subscribe(ORB_ID(actuator_controls_0));

	poll_subscriptions();
	serialdata.initialSerial();



	while (!_task_should_exit) {

		usleep(30000);
		poll_subscriptions();

		if (_serial_start==false)
		{
			 _odroid_preflight_status.odroid_preflight_state=ODROID_PREFLIGHT_STATE_AUTO;


		    serialdata.readMsgFromSerial();


			if(serialdata.status_receive==true&&serialdata.receive.cmd>0)
				{

					_start_behavior=serialdata.receive.cmd;

					switch (_start_behavior)
					{
						case 51:
							if(_vehicle_status.condition_global_position_valid==true)
							{

								serialdata.send.cmd=52;

								serialdata.send.parameters.push_back(_global_pos.lat*10000);
								serialdata.send.parameters.push_back(_global_pos.lon*10000);
								serialdata.send.parameters.push_back(_global_pos.alt);
								serialdata.send.parameters.push_back(4);

								serialdata.writeMsg2Serial();
								serialdata.send.parameters.clear();

								if (serialdata.status_send==true&&serialdata.send.cmd==52)
								{
									_gps_valid=true;
								}

							}
							break;

					case 71:

						serialdata.send.cmd=72;
						serialdata.send.parameters.push_back(_local_pos.x);
						serialdata.send.parameters.push_back(_local_pos.y);
						serialdata.send.parameters.push_back(_local_pos.z);
						serialdata.send.parameters.push_back(_local_pos.yaw);

						serialdata.writeMsg2Serial();
						serialdata.send.parameters.clear();

						if (serialdata.status_send==true&&serialdata.send.cmd==72)
						{
						_location_valid=true;

						serialdata.readMsgFromSerial();
						}

						break;

					case 90:

					_odroid_preflight_status.odroid_preflight_state=ODROID_PREFLIGHT_STATE_INIT;

					serialdata.send.cmd=91;
					serialdata.writeMsg2Serial();
					//
					serialdata.send.parameters.clear();

						break;
					}
				}


			if(_vehicle_status.nav_state==NAVIGATION_STATE_AUTO_ODROID)

			{
				switch (_handshake_state)
				{

				case 1:

					serialdata.send.cmd=1;
					serialdata.send.parameters.clear();

					serialdata.writeMsg2Serial();
					usleep(50000);

					serialdata.readMsgFromSerial();
					if(serialdata.receive.cmd==2){
					_handshake_state=2;
					}

					break;

				case 2:

					serialdata.send.cmd=3;
					serialdata.send.parameters.clear();

					serialdata.writeMsg2Serial();
					usleep(50000);

					serialdata.readMsgFromSerial();
					if(serialdata.receive.cmd==51){
					 _handshake_state=3;
					}

					break;

				case 3:
					_serial_start=true;

					break;



				}

			}

		}


/*handshake done*/
		  if (_serial_start==true)
		  {

			serialdata.readMsgFromSerial();

			if(serialdata.status_receive==true&&serialdata.receive.cmd>0&&_vehicle_status.nav_state==NAVIGATION_STATE_AUTO_ODROID)//&&_vehicle_status.nav_state==NAVIGATION_STATE_AUTO_ODROID
			{


				_odroid_behavior=serialdata.receive.cmd;

				switch (_odroid_behavior)
				{

			 case 51:
			    if(_vehicle_status.condition_global_position_valid==true)
			        {
		                serialdata.send.cmd=52;
					    serialdata.send.parameters.push_back(_global_pos.lat*10000);
					    serialdata.send.parameters.push_back(_global_pos.lon*10000);
					    serialdata.send.parameters.push_back(_global_pos.alt);
					    serialdata.send.parameters.push_back(4);

						serialdata.writeMsg2Serial();
						serialdata.send.parameters.clear();

							if (serialdata.status_send==true&&serialdata.send.cmd==52)
							 {
							    		 _gps_valid=true;


							 }

			        }

			    	break;

		    	case 71:
		    		serialdata.send.cmd=72;
		    		serialdata.send.parameters.push_back(_local_pos.x);
		    	    serialdata.send.parameters.push_back(_local_pos.y);
		    	    serialdata.send.parameters.push_back(_local_pos.z);
		    		serialdata.send.parameters.push_back(_local_pos.yaw);

		    		serialdata.writeMsg2Serial();
		    		serialdata.send.parameters.clear();

		    		 if (serialdata.status_send==true&&serialdata.send.cmd==72)
		    			 {
		    				_location_valid=true;

		    			serialdata.readMsgFromSerial();
		    			 } break;


				case 21:
                    _mea_start=true;


                //    printf ("stop....... \n" );


					break;

				case 28:
					_mea_start=false;

					break;

				case 88:


					_serial_reference.current.cmd=88;
					_serial_reference.current.x=serialdata.receive.parameters[0];
					_serial_reference.current.y=serialdata.receive.parameters[1];
					_serial_reference.current.z=serialdata.receive.parameters[2];
					_serial_reference.current.yaw=serialdata.receive.parameters[3];
					_serial_reference.current.vx=serialdata.receive.parameters[4];
					_serial_reference.current.vy=serialdata.receive.parameters[5];
					_serial_reference.current.vz=serialdata.receive.parameters[6];
					_serial_reference.current.yawspeed=serialdata.receive.parameters[7];
					_serial_reference.current.accx=serialdata.receive.parameters[8];
					_serial_reference.current.accy=serialdata.receive.parameters[9];
					_serial_reference.current.accz=serialdata.receive.parameters[10];//no need accc

					serialdata.receive.parameters.clear();

					break;

				case 250:
//					_mea_start=false;
					serialdata.send.cmd=251;
					serialdata.writeMsg2Serial();
					_odroid_status.odroid_nav_state=ODROID_STATE_LAND;

					break;

				case 100:

					_odroid_preflight_status.odroid_preflight_state=ODROID_PREFLIGHT_STATE_ACTION;


					if(_actuators.control[5]>0)
					{

						serialdata.send.cmd=101;
						serialdata.writeMsg2Serial();
					}

					break;


				}


				if(serialdata.receive_history.size()>=2){
					for(int i=0;i<serialdata.receive_history.size();++i){
						switch (serialdata.receive_history.at(i).cmd){
						case 100:
							_odroid_preflight_status.odroid_preflight_state=ODROID_PREFLIGHT_STATE_ACTION;
							if (_actuators.control[5] > 0) {

								serialdata.send.cmd = 101;
								serialdata.writeMsg2Serial();
							}

							break;
						default:
							break;
						}
					}
				}



				if (_mea_start)
				{

				  serialdata.send.cmd=22;

				  serialdata.send.parameters.push_back(_local_pos.x);
				  serialdata.send.parameters.push_back(_local_pos.y);
				  serialdata.send.parameters.push_back(_local_pos.z);
				  serialdata.send.parameters.push_back(_local_pos.yaw);
				  serialdata.send.parameters.push_back(_local_pos.vx);
				  serialdata.send.parameters.push_back(_local_pos.vy);
				  serialdata.send.parameters.push_back(_local_pos.vz);
				  serialdata.send.parameters.push_back(0);
				  serialdata.send.parameters.push_back(0);
				  serialdata.send.parameters.push_back(0);
				  serialdata.send.parameters.push_back(0);
				  serialdata.send.parameters.push_back(0);
				  serialdata.send.parameters.push_back(_vehicle_attitude.roll);
				  serialdata.send.parameters.push_back(_vehicle_attitude.pitch);
				  serialdata.send.parameters.push_back(_vehicle_attitude.yaw);
								    	//	serialdata.send.parameters.push_back(_local_pos.vz);

				  serialdata.writeMsg2Serial();
				  serialdata.send.parameters.clear();
				  measurement_poll();


				}

			}


//			_odroid_status.odroid_nav_state=ODROID_STATE_TAKEOFF;

		  }


	if (_serial_pub > 0) {
				orb_publish(ORB_ID(position_serialport), _serial_pub, &_serial_reference);

	} else {
		_serial_pub = orb_advertise(ORB_ID(position_serialport), &_serial_reference);
	}

	if (_odroid_status_pub > 0) {
				orb_publish(ORB_ID(odroid_status), _odroid_status_pub, &_odroid_status);


	} else {
		_odroid_status_pub = orb_advertise(ORB_ID(odroid_status), &_odroid_status);

	}

	if (_odroid_preflight_status_pub > 0) {
				orb_publish(ORB_ID(odroid_preflight_status), _odroid_preflight_status_pub, &_odroid_preflight_status);


	} else {
		_odroid_preflight_status_pub = orb_advertise(ORB_ID(odroid_preflight_status), &_odroid_preflight_status);

	}





	}


	_serialport_task = -1;
		_exit(0);

}




int
Serialport::start()
{
	ASSERT(_serialport_task == -1);

	/* start the task */
	_serialport_task = task_spawn_cmd("serialport_task",
				       SCHED_DEFAULT,
				       SCHED_PRIORITY_DEFAULT,
				       2000,
				       (main_t)&Serialport::task_main_trampoline,
				       nullptr);

	if (_serialport_task < 0) {
		warn("task start failed");
		return -errno;
	}

	return OK;
}


int serialport_main(int argc, char *argv[])
{
	if (argc < 1) {
		errx(1, "usage: sensors {start|stop|status}");
	}

	if (!strcmp(argv[1], "start")) {

		if (serialport::g_serialport != nullptr) {
			errx(0, "already running");
		}

		serialport::g_serialport = new Serialport;

		if (serialport::g_serialport == nullptr) {
			errx(1, "alloc failed");
		}

		if (OK != serialport::g_serialport->start()) {
			delete serialport::g_serialport;
			serialport::g_serialport = nullptr;
			err(1, "start failed");
		}

		exit(0);
	}

	if (!strcmp(argv[1], "stop")) {
		if (serialport::g_serialport == nullptr) {
			errx(1, "not running");
		}

		delete serialport::g_serialport;
		serialport::g_serialport = nullptr;
		exit(0);
	}

	if (!strcmp(argv[1], "status")) {
		if (serialport::g_serialport) {
			errx(0, "is running");

		} else {
			errx(1, "not running");
		}
	}

	warnx("unrecognized command");
	return 1;
}

