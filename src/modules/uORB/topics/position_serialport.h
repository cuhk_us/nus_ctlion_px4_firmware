/****************************************************************************
 *
 *   Copyright (C) 2013 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
Hailong
 */

#ifndef TOPIC_SERIALPORT_H_
#define TOPIC_SERIALPORT_H_

#include <stdint.h>
#include <stdbool.h>
#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */


struct position_serialsetpoint_s
{
	uint8_t cmd1;
	uint8_t cmd2;
	uint8_t cmd3;
	bool valid;			/**< true if setpoint is valid */
	float x;			/**< local position setpoint in m in NED */
	float y;			/**< local position setpoint in m in NED */
	float z;			/**< local position setpoint in m in NED */
	bool position_valid;	/**< true if local position setpoint valid */
	float vx;			/**< local velocity setpoint in m/s in NED */
	float vy;			/**< local velocity setpoint in m/s in NED */
	float vz;			/**< local velocity setpoint in m/s in NED */
	float accx;
	float accy;
	float accz;
	bool velocity_valid;		/**< true if local velocity setpoint valid */
	double lat;			/**< latitude, in deg */
	double lon;			/**< longitude, in deg */
	float alt;			/**< altitude AMSL, in m */
	float yaw;			/**< yaw (only for multirotors), in rad [-PI..PI), NaN = hold current yaw */
	float yawspeed;			/**< yawspeed (only for multirotors, in rad/s) */
	float loiter_radius;		/**< loiter radius (only for fixed wing), in m */
	int8_t loiter_direction;	/**< loiter direction: 1 = CW, -1 = CCW */
	float pitch_min;		/**< minimal pitch angle for fixed wing takeoff waypoints */
	int   nav_frame;
};

/**
 * Global position setpoint triplet in WGS84 coordinates.
 *
 * This are the three next waypoints (or just the next two or one).
 */
struct position_serialport_s
{
	struct position_serialsetpoint_s current;
	unsigned nav_state;				/**< report the navigation state */
	float servo1;
	float servo2;
	float servo3;
	float servo4;
};

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(position_serialport);

#endif
