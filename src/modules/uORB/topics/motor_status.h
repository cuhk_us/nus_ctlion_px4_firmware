/*
 * motor_status.h
 *
 *  Created on: Dec 15, 2014
 *      Author: NUS_UAV
 */

#ifndef MOTOR_STATUS_H_
#define MOTOR_STATUS_H_


#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */

struct motor_status_s {
	uint64_t t_timestamp;
	bool motor_fault;
	bool motor1;
	bool motor2;
	bool motor3;
	bool motor4;
}; /**< Velocity setpoint in NED frame */

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(motor_status);


#endif /* MOTOR_STATUS_H_ */
