/****************************************************************************
 *
 *   Copyright (C) 2013, 2014 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file position_estimator_inav_main.c
 * Model-identification based position estimator for multirotors
 *
 * @author Anton Babushkin <anton.babushkin@me.com>
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <nuttx/config.h>
#include <nuttx/sched.h>
#include <sys/prctl.h>
#include <termios.h>
#include <math.h>
#include <float.h>
#include <uORB/uORB.h>
#include <uORB/topics/parameter_update.h>
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/actuator_armed.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/laser_position_estimate.h>
#include <uORB/topics/home_position.h>
#include <drivers/drv_range_finder.h>
#include <uORB/topics/manual_control_setpoint.h>
//#include <uORB/topics/debug_test.h>
//#include <lib/PosKalman/PosKalman.h>
#include <systemlib/param/param.h>
#include <mavlink/mavlink_log.h>
#include <poll.h>
#include <systemlib/err.h>
#include <geo/geo.h>
#include <systemlib/systemlib.h>
#include <mathlib/mathlib.h>
#include <drivers/drv_hrt.h>

#define MIN_VALID_R 0.0000001f
#define PUB_INTERVAL 10000	// limit publish rate to 100 Hz
#define EST_BUF_SIZE 250000 / PUB_INTERVAL		// buffer size is 0.25s



#define gps_topic_timeout 1000000		// GPS topic timeout = 1.0s
#define nuc_topic_timeout 1000000	// nuc pos estimator topic timeout = 1s
#define distance_sensor_timeout  500000	// sonar timeout = 0.5s
#define distance_sensor_valid_timeout 1000000	// estimate distance during this time after distance sensor loss
#define updates_counter_len  1000000
#define external_distance_sensor_timeout 200000 // distance sensor timeout 200ms, so that the short time cross over of different height will not affect the filter



extern "C" __EXPORT int position_estimator_inav_main(int argc, char *argv[]);


class PositionEstimatorInav{
public:
	PositionEstimatorInav();

	~PositionEstimatorInav();

	int		start();



private:
	bool	_task_should_exit;		/**< if true, task should exit */
	int		_position_estimator_inav_task;
	bool 	_verbose_mode;

	int mavlink_fd;


	int buf_ptr;

	bool external_distance_valid;
	bool external_pos_source_valid;
	bool can_estimate_xy;
	bool can_estimate_z;

	hrt_abstime external_distance_valid_time;

	float z_baro;
	float Tao_baro;
	float baro_beta;
	float Tao_bias;
	float Tao_acc;

	float dt;
	int loop_count;

	bool gps_ref_inited;
	bool xy_ref_inited;
	bool z_ref_inited;




	// kalman filter variables
	math::Vector<3> _Xx;		// state: _Xx(0) x pos, _Xx(1) x velocity,  _Xx(2) x accel bias
	math::Matrix<3, 3> _Px;	// covariance matrix


	math::Vector<3> _Xy;		// state: _Xy(0) y pos, _Xy(1) y velocity,  _Xy(2) y accel bias
	math::Matrix<3, 3> _Py;	// covariance matrix

	math::Vector<3> _Xz; 		// state: _Xz(0) z pos, _Xz(1) z velocity,  _Xz(2) z accel bias _Xz(3) baro offset, z_baro = _Xz(0) + _Xz(3);
	math::Matrix<3, 3> _Pz;//



	/* subscribe */
	int parameter_update_sub;
	int actuator_sub;
	int armed_sub;
	int sensor_combined_sub;
	int vehicle_attitude_sub;
	int vehicle_gps_position_sub;
	int home_position_sub;
	int laser_position_estimate_sub;
	int distance_sensor_sub;
	int	manual_control_setpoint_sub;

	/* advertise */
	orb_advert_t vehicle_local_position_pub;
	orb_advert_t vehicle_global_position_pub;
	orb_advert_t debug_pub;


	float x_est[2];	// pos, vel
	float y_est[2];	// pos, vel
	float z_est[2];	// pos, vel

	float est_buf[EST_BUF_SIZE][3][2];	// estimated position buffer
	float R_buf[EST_BUF_SIZE][3][3];	// rotation matrix buffer
	float angular_speed_buf[EST_BUF_SIZE][3];
	float R_gps[3][3];					// rotation matrix for GPS correction moment
	float angular_speed_gps[3];			// angular speed estimation at the GPC correction moment

	float acc[3];

	float baro_cur_reading_filtered,baro_pre_reading_filtered,baro_velocity_filtered;
	float distance_cur_reading_filtered, distance_pre_reading_filtered, distance_velocity_filtered; // added by Mingjie and Lin Feng on 6 July for z velocity estimation using laser distance

	/* declare and safely initialize all structs */
	struct actuator_controls_s actuator;
	struct actuator_armed_s armed;
	struct sensor_combined_s sensor;
	struct vehicle_gps_position_s gps;
//	struct home_position_s home;
	struct vehicle_attitude_s att;
	struct vehicle_local_position_s local_pos;
	struct vehicle_global_position_s global_pos;
	struct laser_position_estimate laser_position_estimate;
	struct range_finder_report distance_z;
	struct map_projection_reference_s gps_ref;
	struct manual_control_setpoint_s man_sp;



	static void	task_main_trampoline(int argc, char *argv[]);
	void		task_main();

	int parameters_update();
	void inertial_filter_predict(float dt, float x[2], float acc);
	void inertial_filter_correct(float e, float dt, float x[2], int i, float w);

	void xy_predict(float acc_x, float acc_y);
	void xy_pos_update(float x_pos, float y_pos, float R_pos);
	void xy_vel_update(float x_vel, float y_vel, float R_vel);

	void xy_init(float x, float vx, float y, float vy);


	void z_predict(float acc);
	void z_pos_update(float z_pos, float R_pos);
	void z_vel_update(float z_vel, float R_vel);
//	void z_baro_update(float baro_h, float R_baro);

	// Related to baro bandpass filter
	float bandpass_filter(float new_data, float *pre_data, float *input_data, float *A, float *B);

	float lowpass_filter(float new_data, float *pre_data, float *input_data, float *A, float *B);


	void z_init(float z, float vz);

	struct  {
		float r_z_baro;
		float tao_baro;
		float baro_beta;
		float r_z_gps_p;
		float r_z_gps_v;
		float r_z_vision_p;
		float r_xy_gps_p;
		float r_xy_gps_v;
		float r_xy_vision_p;
		float r_xy_vision_v;
		float r_xy_nuc_p;
		float r_xy_nuc_v;
		float r_z_nuc_p;
		float r_z_nuc_v;
		float r_xy_flow;
		float r_xy_res_v;
		float r_gps_flow;
		float q_acc_bias;
		float q_acc;
		float flow_k;
		float flow_q_min;
		float nuc_filt;
		float nuc_err;
		float land_t;
		float land_disp;
		float land_thr;
		float in_air_thr;
		int32_t no_vision;
		float delay_gps;
		float delay_mea;
		int32_t simulation;
		float gps_antenna_offset[3];
		int32_t   use_distance;
		int32_t att_corr_nuc_z;
		float distance_filt;
		float distance_err;
		float r_z_distance_p;
		int32_t use_baro;
		int32_t use_nuc_z;
		float camera_tilt_angle;
		int32_t use_distance_vel;
		float r_z_distance_vel;
	}params;

	struct  {
		param_t r_z_baro;
		param_t tao_baro;
		param_t baro_beta;
		param_t r_z_gps_p;
		param_t r_z_vision_p;
		param_t r_xy_gps_p;
		param_t r_xy_gps_v;
		param_t r_xy_vision_p;
		param_t r_xy_vision_v;
		param_t r_xy_nuc_p;
		param_t r_xy_nuc_v;
		param_t r_z_nuc_p;
		param_t r_z_nuc_v;
		param_t r_z_distance_p;
		param_t r_xy_flow;
		param_t r_xy_res_v;
		param_t r_gps_flow;
		param_t q_acc_bias;
		param_t q_acc;
		param_t flow_k;
		param_t flow_q_min;
		param_t nuc_filt;
		param_t nuc_err;
		param_t land_t;
		param_t land_disp;
		param_t land_thr;
		param_t in_air_thr;
		param_t no_vision;
		param_t delay_gps;
		param_t delay_mea;
		param_t r_z_gps_v;
		param_t simulation;
		param_t gps_off_x;
		param_t gps_off_y;
		param_t gps_off_z;
		param_t use_distance;
		param_t att_corr_nuc_z;
		param_t distance_filt;
		param_t distance_err;
		param_t use_baro;
		param_t use_nuc_z;
		param_t camera_tilt_angle;
		param_t use_distance_vel;
		param_t r_z_distance_vel;
	}param_handles;

};


namespace Position_estimate
{
	PositionEstimatorInav	*pos_estimate;
}



/**
 * The position_estimator_inav_thread only briefly exists to start
 * the background job. The stack size assigned in the
 * Makefile does only apply to this management task.
 *
 * The actual stack size should be set in the call
 * to task_create().
 */

void
PositionEstimatorInav::task_main_trampoline(int argc, char *argv[])
{
	Position_estimate::pos_estimate->task_main();
}


PositionEstimatorInav::PositionEstimatorInav() :
	_task_should_exit(false),
	_position_estimator_inav_task(-1),
	_verbose_mode(false),
	mavlink_fd(-1),
	buf_ptr(0),
	external_distance_valid(false),
	external_pos_source_valid(false),
	can_estimate_xy(false),
	can_estimate_z(false),
	z_baro(0.0f),
	Tao_baro(5.0f),
	baro_beta(0.5f),
	Tao_bias(0.000001f),
	Tao_acc(0.09f),
	dt(0),
	loop_count(0),
	gps_ref_inited(false),
	xy_ref_inited(false),
	z_ref_inited(false)
	{

	_Xx.zero();
	_Xy.zero();
	_Xz.zero();

	_Px.zero();
	_Py.zero();
	_Pz.zero();

	memset(x_est,0,sizeof(x_est));
	memset(y_est,0,sizeof(y_est));
	memset(z_est,0,sizeof(z_est));

	memset(est_buf, 0, sizeof(est_buf));
	memset(R_buf, 0, sizeof(R_buf));
	memset(angular_speed_buf,0,sizeof(angular_speed_buf));
	memset(R_gps, 0, sizeof(R_gps));
	memset(angular_speed_gps,0,sizeof(angular_speed_gps));
	memset(acc,0,sizeof(acc));
	baro_cur_reading_filtered = 0.0f;
	baro_pre_reading_filtered = 0.0f;
	baro_velocity_filtered = 0.0f;

	distance_cur_reading_filtered = 0.0f; // initialization
	distance_pre_reading_filtered = 0.0f;
	distance_velocity_filtered = 0.0f;


	/* declare and safely initialize all structs */
	memset(&actuator, 0, sizeof(actuator));
	memset(&armed, 0, sizeof(armed));
	memset(&sensor, 0, sizeof(sensor));
	memset(&gps, 0, sizeof(gps));
//	memset(&home, 0, sizeof(home));
	memset(&att, 0, sizeof(att));
	memset(&local_pos, 0, sizeof(local_pos));
	memset(&global_pos, 0, sizeof(global_pos));
	memset(&laser_position_estimate, 0, sizeof(laser_position_estimate));
	memset(&gps_ref,0,sizeof(gps_ref));
	memset(&man_sp,0,sizeof(man_sp));


	param_handles.r_z_baro = param_find("INAV_R_Z_BARO");
	param_handles.tao_baro = param_find("INAV_TAO_BARO");
	param_handles.baro_beta = param_find("INAV_BARO_BETA");
	param_handles.r_z_gps_p = param_find("INAV_R_Z_GPS_P");
	param_handles.r_z_vision_p = param_find("INAV_R_Z_VIS_P");
	param_handles.r_z_gps_v = param_find("INAV_R_Z_GPS_V");
	param_handles.r_z_nuc_p = param_find("INAV_R_Z_MEA_P");
	param_handles.r_z_nuc_v = param_find("INAV_R_Z_MEA_V");
	param_handles.distance_filt = param_find("INAV_DIST_FILT");
	param_handles.distance_err = param_find("INAV_DIST_ERR");
	param_handles.use_baro = param_find("INAV_USE_BARO");
	param_handles.use_nuc_z = param_find("INAV_USE_NUC_Z");
	param_handles.camera_tilt_angle = param_find("INAV_CAMERA_TILT");
	param_handles.r_z_distance_p = param_find("INAV_R_Z_DIST");
	param_handles.nuc_filt = param_find("INAV_LASER_FILT");
	param_handles.nuc_err = param_find("INAV_LASER_ERR");
	param_handles.use_distance_vel = param_find("INAV_USE_DIST_V");
	param_handles.r_z_distance_vel = param_find("INAV_R_Z_DIST_V");

	param_handles.r_xy_gps_p = param_find("INAV_R_XY_GPS_P");
	param_handles.r_xy_gps_v = param_find("INAV_R_XY_GPS_V");
	param_handles.r_xy_vision_p = param_find("INAV_R_XY_VIS_P");
	param_handles.r_xy_vision_v = param_find("INAV_R_XY_VIS_V");
	param_handles.r_xy_nuc_p = param_find("INAV_R_XY_MEA_P");
	param_handles.r_xy_nuc_v = param_find("INAV_R_XY_MEA_V");

	param_handles.r_xy_flow = param_find("INAV_R_XY_FLOW");
	param_handles.r_xy_res_v = param_find("INAV_R_XY_RES_V");
	param_handles.r_gps_flow = param_find("INAV_R_GPS_FLOW");
	param_handles.q_acc_bias = param_find("INAV_Q_ACC_BIAS");
	param_handles.q_acc = param_find("INAV_Q_ACC");

	param_handles.flow_k = param_find("INAV_FLOW_K");
	param_handles.flow_q_min = param_find("INAV_FLOW_Q_MIN");


	param_handles.land_t = param_find("INAV_LAND_T");
	param_handles.land_disp = param_find("INAV_LAND_DISP");
	param_handles.land_thr = param_find("INAV_LAND_THR");
	param_handles.in_air_thr = param_find("INAV_IN_AIR_THR");
	param_handles.no_vision = param_find("CBRK_NO_VISION");
	param_handles.delay_gps = param_find("INAV_DELAY_GPS");
	param_handles.delay_mea = param_find("INAV_DELAY_MEA");

	param_handles.gps_off_x = param_find("INAV_GPS_OFFX");
	param_handles.gps_off_y = param_find("INAV_GPS_OFFY");
	param_handles.gps_off_z = param_find("INAV_GPS_OFFZ");
	param_handles.use_distance = param_find("INAV_USE_DIST");
	param_handles.att_corr_nuc_z = param_find("NUC_ATT_CORR_Z");


	}

PositionEstimatorInav::~PositionEstimatorInav()
{
	if(_position_estimator_inav_task != -1)
	{
		_task_should_exit = true;

		unsigned i = 0;

		do {
			/* wait 20ms */
			usleep(20000);

			/* if we have given up, kill it */
			if (++i > 50) {
				task_delete(_position_estimator_inav_task);
				break;
			}
		} while (_position_estimator_inav_task != -1);

	}

	Position_estimate::pos_estimate = nullptr;
}


//static void write_debug_log(const char *msg, float dt, float x_est[2], float y_est[2], float z_est[2], float x_est_prev[2], float y_est_prev[2], float z_est_prev[2], float acc[3], float corr_gps[3][2], float w_xy_gps_p, float w_xy_gps_v)
//{
//	FILE *f = fopen("/fs/microsd/inav.log", "a");
//
//	if (f) {
//		char s[256];
//		unsigned n = snprintf(s, 256, "%llu %s\n\tdt=%.5f x_est=[%.5f %.5f] y_est=[%.5f %.5f] z_est=[%.5f %.5f] x_est_prev=[%.5f %.5f] y_est_prev=[%.5f %.5f] z_est_prev=[%.5f %.5f]\n",
//                              hrt_absolute_time(), msg, (double)dt,
//                              (double)x_est[0], (double)x_est[1], (double)_Xy(0), (double)_Xy(1), (double)_Xz(0), (double)_Xz(1),
//                              (double)x_est_prev[0], (double)x_est_prev[1], (double)y_est_prev[0], (double)y_est_prev[1], (double)z_est_prev[0], (double)z_est_prev[1]);
//		fwrite(s, 1, n, f);
//		n = snprintf(s, 256, "\tacc=[%.5f %.5f %.5f] gps_pos_corr=[%.5f %.5f %.5f] gps_vel_corr=[%.5f %.5f %.5f] w_xy_gps_p=%.5f w_xy_gps_v=%.5f\n",
//                     (double)acc[0], (double)acc[1], (double)acc[2],
//                     (double)corr_gps[0][0], (double)corr_gps[1][0], (double)corr_gps[2][0], (double)corr_gps[0][1], (double)corr_gps[1][1], (double)corr_gps[2][1],
//                     (double)w_xy_gps_p, (double)w_xy_gps_v);
//		fwrite(s, 1, n, f);
//		free(s);
//	}
//
//	fsync(fileno(f));
//	fclose(f);
//}



int PositionEstimatorInav::parameters_update()
{
	param_get(param_handles.r_z_baro, &(params.r_z_baro));
	param_get(param_handles.tao_baro, &(params.tao_baro));
	param_get(param_handles.baro_beta, &(params.baro_beta));
	param_get(param_handles.r_z_gps_p, &(params.r_z_gps_p));
	param_get(param_handles.r_z_vision_p, &(params.r_z_vision_p));
	param_get(param_handles.r_xy_gps_p, &(params.r_xy_gps_p));
	param_get(param_handles.r_xy_gps_v, &(params.r_xy_gps_v));
	param_get(param_handles.r_xy_vision_p, &(params.r_xy_vision_p));
	param_get(param_handles.r_xy_vision_v, &(params.r_xy_vision_v));
	param_get(param_handles.r_xy_nuc_p, &(params.r_xy_nuc_p));
	param_get(param_handles.r_xy_nuc_v, &(params.r_xy_nuc_v));
	param_get(param_handles.r_z_nuc_p, &(params.r_z_nuc_p));
	param_get(param_handles.r_z_nuc_v, &params.r_z_nuc_v);
	param_get(param_handles.r_xy_flow, &(params.r_xy_flow));
	param_get(param_handles.r_xy_res_v, &(params.r_xy_res_v));
	param_get(param_handles.r_gps_flow, &(params.r_gps_flow));
	param_get(param_handles.q_acc_bias, &(params.q_acc_bias));
	param_get(param_handles.q_acc, &(params.q_acc));
	param_get(param_handles.flow_k, &(params.flow_k));
	param_get(param_handles.flow_q_min, &(params.flow_q_min));
	param_get(param_handles.nuc_filt,&(params.nuc_filt));
	param_get(param_handles.nuc_err,&(params.nuc_err));
	param_get(param_handles.land_t, &(params.land_t));
	param_get(param_handles.land_disp, &(params.land_disp));
	param_get(param_handles.land_thr, &(params.land_thr));
	param_get(param_handles.in_air_thr,&(params.in_air_thr));
	param_get(param_handles.no_vision, &(params.no_vision));
	param_get(param_handles.delay_gps, &(params.delay_gps));
	param_get(param_handles.delay_mea,&(params.delay_mea));
	param_get(param_handles.r_z_gps_v,&(params.r_z_gps_v));
	param_get(param_handles.gps_off_x,&(params.gps_antenna_offset[0]));
	param_get(param_handles.gps_off_y,&(params.gps_antenna_offset[1]));
	param_get(param_handles.gps_off_z,&(params.gps_antenna_offset[2]));
	param_get(param_handles.use_distance,&(params.use_distance));
	param_get(param_handles.att_corr_nuc_z,&(params.att_corr_nuc_z));
	param_get(param_handles.distance_filt, &(params.distance_filt));
	param_get(param_handles.distance_err, &(params.distance_err));
	param_get(param_handles.r_z_distance_p, &(params.r_z_distance_p));
	param_get(param_handles.use_baro,&(params.use_baro));
	param_get(param_handles.use_nuc_z,&(params.use_nuc_z));
	param_get(param_handles.camera_tilt_angle,&(params.camera_tilt_angle));
	param_get(param_handles.use_distance_vel,&(params.use_distance_vel));
	param_get(param_handles.r_z_distance_vel,&(params.r_z_distance_vel));

	Tao_baro = params.tao_baro;
	baro_beta = params.baro_beta;
	Tao_bias = params.q_acc_bias;
	Tao_acc = params.q_acc;

	return OK;
}



void PositionEstimatorInav::xy_init(float x, float vx, float y, float vy)
{
	_Xx(0) = x;
	_Xx(1) = vx;
	_Xx(2) = 0;

	_Xy(0) = y;
	_Xy(1) = vy;
	_Xy(2) = 0;
	_Px.zero();
	_Py.zero();
}

void PositionEstimatorInav::z_init(float z, float vz)
{
//	printf("In z init, z is %8.4lf vz is %8.4lf h baro is %8.4lf\n",(double)z,(double)vz,(double)h_baro);

	_Xz(0) = z;
	_Xz(1) = vz;
	_Xz(2) = 0; // init the baro offset

	_Pz.zero();
//	printf("_Xz is ####\n");
//	_Xz.print();
}

void PositionEstimatorInav::xy_predict(float acc_x, float acc_y)
{
	float dt2 = dt  * dt;
	float dt3 = dt2 * dt;
	float dt4 = dt3 * dt;
	float dt5 = dt4 * dt;

	math::Matrix<3,3> H;
	H.identity();
	H(0,1) =  dt;
	H(0,2) = -dt2/2;
	H(1,2) = -dt;

	math::Matrix<3,3> Q;
	Q(0,0) = dt3/3*Tao_acc+dt5/20*Tao_bias;
	Q(0,1) = dt2/2*Tao_acc+dt4/8*Tao_bias;
	Q(0,2) = -dt3/6*Tao_bias;

	Q(1,0) = dt2/2*Tao_acc+dt4/8 *Tao_bias;
	Q(1,1) = dt*Tao_acc+dt3/3*Tao_bias;
	Q(1,2) = -dt2/2*Tao_bias;

	Q(2,0) = -dt3/6*Tao_bias;
	Q(2,1) = -dt2/2*Tao_bias;
	Q(2,2) =  dt*Tao_bias;

	math::Vector<3> T;
	T(0) = dt2/2;
	T(1) = dt;
	T(2) = 0;

	_Xx = H * _Xx + T * acc_x;
	_Px = H * _Px * H.transposed() + Q;

	_Xy = H * _Xy + T * acc_y;
	_Py = H * _Py * H.transposed() + Q;
}

void PositionEstimatorInav::xy_pos_update(float corr_x, float corr_y, float R_pos)
{

	//------For x direction ---------------//
	//--------------------------------------//

	float S = _Px(0,0) + R_pos; //S = C1 * P_ * C1' + R1;
	math::Vector<3> C;
	C.zero();
	C(0) = 1;
	math::Vector<3> K;



	// prevent S from being 0
	if(fabsf(S) < 0.000001f)
		S = 0.000001f;


	K = _Px * C /S; //K1 = P_ * C1' / S;
	_Xx += K * corr_x; // dx_ = dx_ + K1 * (z1_ - C1 * dx_);

	math::Matrix<3,3> KC;
	KC.zero();

	KC(0,0) = K(0);
	KC(1,0) = K(1);
	KC(2,0) = K(2);

	_Px -= KC*_Px; //P_ = P_ - K1 * C1 * P_;



	//------For y direction ---------------//
	//--------------------------------------//

	S = _Py(0,0) + R_pos;

	// prevent S from being 0
	if(fabsf(S) < 0.000001f)
		S = 0.000001f;

	K = _Py * C /S; //K1 = P_ * C1' / S;
	_Xy += K * corr_y; // dx_ = dx_ + K1 * (z1_ - C1 * dx_);

	KC.zero();

	KC(0,0) = K(0);
	KC(1,0) = K(1);
	KC(2,0) = K(2);

	_Py -= KC*_Py; //P_ = P_ - K1 * C1 * P_;

}

void PositionEstimatorInav::xy_vel_update(float corr_x_vel, float corr_y_vel, float R_vel)
{

//	if(loop_count % 200 == 0)
//		printf("corr vx is %8.4lf correct vy is %8.4lf, R is %8.4lf\n",(double)corr_x_vel,(double)corr_y_vel,(double)R_vel);


	float S = _Px(1,1) + R_vel; //S = C1 * P_ * C1' + R1; C1 = [ 0 1 0]
	math::Vector<3> C;
	C.zero();
	C(1) = 1;
	math::Vector<3> K;

	// prevent S from being 0
	if(fabsf(S) < 0.000001f)
		S = 0.000001f;

	K = _Px * C / S; //K1 = P_ * C1' / S;
	_Xx += K * corr_x_vel; // dx_ = dx_ + K1 * (z1_ - C1 * dx_);

	math::Matrix<3,3> KC;
	KC.zero();

	KC(0,1) = K(0);
	KC(1,1) = K(1);
	KC(2,1) = K(2);

	_Px -= KC*_Px;

	//----------For update y vel -------------//
	//----------------------------------------//


	S = _Py(1,1) + R_vel;
	// prevent S from being 0
	if(fabsf(S) < 0.000001f)
		S = 0.000001f;

	K = _Py * C /S; //K1 = P_ * C1' / S;
	_Xy += K * corr_y_vel; // dx_ = dx_ + K1 * (z1_ - C1 * dx_);

	KC.zero();

	KC(0,1) = K(0);
	KC(1,1) = K(1);
	KC(2,1) = K(2);

	_Py -= KC*_Py;

}

void PositionEstimatorInav::z_predict(float acc_z)
{
	float dt2 = dt  * dt;
	float dt3 = dt2 * dt;
	float dt4 = dt3 * dt;
	float dt5 = dt4 * dt;

	math::Matrix<3,3> H;
	H.identity();
	H(0,1) =  dt;
	H(0,2) = -dt2/2;
	H(1,2) = -dt;

	math::Matrix<3,3> Q;
	Q.zero();
	Q(0,0) = dt3/3*Tao_acc+dt5/20*Tao_bias;
	Q(0,1) = dt2/2*Tao_acc+dt4/8*Tao_bias;
	Q(0,2) = -dt3/6*Tao_bias;

	Q(1,0) = dt2/2*Tao_acc+dt4/8 *Tao_bias;
	Q(1,1) = dt*Tao_acc+dt3/3*Tao_bias;
	Q(1,2) = -dt2/2*Tao_bias;

	Q(2,0) = -dt3/6*Tao_bias;
	Q(2,1) = -dt2/2*Tao_bias;
	Q(2,2) =  dt*Tao_bias;



//	if(loop_count % 20 == 0)
//	{
//		printf("In baro update xz is \n");
//		_Xz.print();
//	}

	math::Vector<3> T;
	T(0) = dt2/2;
	T(1) = dt;
	T(2) = 0;


	_Xz = H * _Xz + T * acc_z;
	_Pz = H * _Pz * H.transposed() + Q;

//	if(loop_count % 20 == 0)
//	{
//		printf("In baro update after updating xz is \n");
//		_Xz.print();
//	}


}

void PositionEstimatorInav::z_pos_update(float corr_z, float R_pos)
{
//	printf("In pos update####\n");
//	printf("R_pos is %8.4lf\n",(double)R_pos);



	float S = _Pz(0,0) + R_pos; //S = C1 * P_ * C1' + R1;

	// prevent S from being 0
	if(fabsf(S) < 0.000001f)
		S = 0.000001f;

	math::Vector<3> C;
	C.zero();
	C(0) = 1;
	math::Vector<3> K;

	K = _Pz * C /S; //K1 = P_ * C1' / S;
	_Xz += K * corr_z; // dx_ = dx_ + K1 * (z1_ - C1 * dx_);

//	printf("S is %8.4f\n",(double)S);
//	printf("K is ##\n");
//	K.print();
//
//	printf("\n\n");


	math::Matrix<3,3> KC;
	KC.zero();

	KC(0,0) = K(0);
	KC(1,0) = K(1);
	KC(2,0) = K(2);

	_Pz -= KC*_Pz;
}



float PositionEstimatorInav::bandpass_filter(float new_data, float *y, float *input, float *A, float *B)
{
	float result = -A[1]*y[0] - A[2] * y[1] - A[3] * y[2] - A[4] * y[3] + B[0] * new_data + B[1] * input[0] + B[2] * input[1] + B[3] * input[2] + B[4] * input[3];
	input[3] = input[2];
	input[2] = input[1];
	input[1] = input[0];
	input[0] = new_data;

	y[3] = y[2];
	y[2] = y[1];
	y[1] = y[0];
	y[0] = result;

	return result;
}

float PositionEstimatorInav::lowpass_filter(float new_data, float *y, float *input, float *A, float *B)
{
	float result = -A[1]*y[0] - A[2] * y[1] + B[0] * new_data + B[1] * input[0] + B[2] * input[1];

	input[1] = input[0];
	input[0] = new_data;

	y[1] = y[0];
	y[0] = result;
	return result;
}



void PositionEstimatorInav::z_vel_update(float corr_z_vel, float R_vel)
{
	float S = _Pz(1,1) + R_vel; //S = C1 * P_ * C1' + R1; C1 = [ 0 1 0]
	math::Vector<3> C;
	C(1) = 1;
	math::Vector<3> K;

	// prevent S from being 0
	if(fabsf(S) < 0.000001f)
		S = 0.000001f;

	K = _Pz * C / S; //K1 = P_ * C1' / S;
	_Xz += K * corr_z_vel; // dx_ = dx_ + K1 * (z1_ - C1 * dx_);

	math::Matrix<3,3> KC;
	KC.zero();

	KC(0,1) = K(0);
	KC(1,1) = K(1);
	KC(2,1) = K(2);

	_Pz -= KC*_Pz;
}



/****************************************************************************
 * main
 ****************************************************************************/
void PositionEstimatorInav::task_main()
{
	warnx("started");

	mavlink_fd = open(MAVLINK_LOG_DEVICE, 0);
	mavlink_log_info(mavlink_fd, "[inav] started");




//	PosKalman KF;

	float min_eph_epv = 2.0f;	// min EPH/EPV, used for weight calculation
	float max_eph_epv = 20.0f;	// max EPH/EPV acceptable for estimation


	float eph = max_eph_epv;
	float epv = 1.0f;


	float x_est_prev[2], y_est_prev[2], z_est_prev[2];
	memset(x_est_prev, 0, sizeof(x_est_prev));
	memset(y_est_prev, 0, sizeof(y_est_prev));
	memset(z_est_prev, 0, sizeof(z_est_prev));

	int baro_init_cnt = 0;
	int baro_init_num = 2000;
	float baro_offset = 0.0f;		// baro offset for reference altitude, initialized on start, then adjusted
	float gps_offset = 0.0f;
	float surface_offset = 0.0f;	// ground level offset from reference altitude
//	float surface_offset_rate = 0.0f;	// surface offset change rate
	float alt_avg = 0.0f;
	bool landed = true;
	hrt_abstime landed_time = 0;

	hrt_abstime accel_timestamp = 0;
	hrt_abstime baro_timestamp = 0;

    hrt_abstime mavlink_open_time = 0;
    const hrt_abstime mavlink_open_interval = 500000;

	hrt_abstime ref_init_start = 0;
	const hrt_abstime ref_init_delay = 1000000;	// wait for 1s after 3D fix
//	hrt_abstime home_timestamp = 0;

	uint16_t accel_updates = 0;
	uint16_t baro_updates = 0;
	uint16_t gps_updates = 0;
	uint16_t attitude_updates = 0;

	hrt_abstime updates_counter_start = hrt_absolute_time();
	hrt_abstime pub_last = hrt_absolute_time();

	hrt_abstime t = 0;
	hrt_abstime t_prev = 0;

	hrt_abstime dist_t = 0;            // time stamp for the distance measurement
	hrt_abstime dist_t_prev = 0;
	float dist_dt = 0.0f;

	float corr_gps[3][2] = {
		{ 0.0f, 0.0f },		// N (pos, vel)
		{ 0.0f, 0.0f },		// E (pos, vel)
		{ 0.0f, 0.0f },		// D (pos, vel)
	};
	float w_gps_xy = 1.0f;
	float w_gps_z = 1.0f;
    float gps_ant_offset_ned[3] = {0.0f,0.0f,0.0f};
    float gps_rotation_vel[3] = {0.0f,0.0f,0.0f};
    float gps_rotation_vel_ned[3] = {0.0f,0.0f,0.0f}; //GPS antena offset in ned frame

	float corr_nuc[3][2] = {
		{ 0.0f, 0.0f },		// N (pos, vel)
		{ 0.0f, 0.0f },		// E (pos, vel)
		{ 0.0f, 0.0f },		// D (pos, vel)
	};

	//for bandpass filter
//	float pre_data[4] = {0.0f,0.0f,0.0f,0.0f};
//	float input_data[4] = {0.0f,0.0f,0.0f,0.0f};
//	float A[5] =   {1.0f,  -3.870744892968549f,   5.620637324473821f,  -3.629003987921737f,   0.879111638724595f};
//	float B[5] =   { 0.001948309358629f,                   0,  -0.003896618717257f,                   0,   0.001948309358629f};

	// for low pass filter
	float pre_data[2] = {0.0f, 0.0f};
	float input_data[2] = {0.0f, 0.0f};
	float A[3] = {1.000000000000000f,  -1.911197067426073f,   0.914975834801433f};
	float B[3] = {0.000944691843840f,   0.001889383687680f,   0.000944691843840f};

	float distance_pre_data[2] = {0.0f, 0.0f};   // for low pass filter used for the distance measurement
	float distance_input_data[2] = {0.0f, 0.0f};

	float corr_distance = 0.0f;
	int corr_distance_outrange_count = 0;
	int	corr_nuc_outrange_count = 0;

	bool distance_height_valid = false;
	bool use_nuc_z = false;
	bool use_nuc_z_pre = false;
	bool use_distance_z = false; // indication of the distance height measurement validness for height estimation
	bool use_distance_z_prev = false;

	hrt_abstime distance_sensor_valid_time = 0;	// time of last sonar measurement used for correction (filtered)
	hrt_abstime distance_sensor_time = 0;

	bool gps_valid = false;			// GPS is valid
	bool nuc_valid = false;
	bool nuc_height_valid = false;



	/* subscribe */
	parameter_update_sub = orb_subscribe(ORB_ID(parameter_update));
	actuator_sub = orb_subscribe(ORB_ID(actuator_controls_1));
	armed_sub = orb_subscribe(ORB_ID(actuator_armed));
	sensor_combined_sub = orb_subscribe(ORB_ID(sensor_combined));
	vehicle_attitude_sub = orb_subscribe(ORB_ID(vehicle_attitude));
	vehicle_gps_position_sub = orb_subscribe(ORB_ID(vehicle_gps_position));
//	home_position_sub = orb_subscribe(ORB_ID(home_position));
	laser_position_estimate_sub = orb_subscribe(ORB_ID(laser_position_estimate));
	distance_sensor_sub = orb_subscribe(ORB_ID(sensor_range_finder));
	manual_control_setpoint_sub = orb_subscribe(ORB_ID(manual_control_setpoint));

	/* advertise */
    vehicle_local_position_pub = orb_advertise(ORB_ID(vehicle_local_position), &local_pos);
    vehicle_global_position_pub = -1;

	/* first parameters update */
	parameters_update();

	struct pollfd fds_init[1];

	fds_init[0].fd = sensor_combined_sub;
	fds_init[0].events = POLLIN;



    loop_count = 0;

	/* wait for initial baro value */
	bool wait_baro = true;


	while (wait_baro && !_task_should_exit) {
		int ret = poll(fds_init, 1, 1000);

		if (ret < 0) {
			/* poll error */

		} else if (ret > 0) {
			if (fds_init[0].revents & POLLIN) {
				orb_copy(ORB_ID(sensor_combined), sensor_combined_sub, &sensor);

				if (wait_baro && sensor.baro_timestamp != baro_timestamp) {
					baro_timestamp = sensor.baro_timestamp;

					/* mean calculation over several measurements */
					if (baro_init_cnt < baro_init_num) {
						if (isfinite(sensor.baro_alt_meter)) {
							baro_offset += sensor.baro_alt_meter;
							baro_init_cnt++;
						}

					} else {
						wait_baro = false;
						baro_offset /= (float) baro_init_cnt;
						z_baro = -(sensor.baro_alt_meter - baro_offset);
						warnx("baro offs: %.2f", (double)baro_offset);
						mavlink_log_info(mavlink_fd, "[inav] baro offs: %.2f", (double)baro_offset);
						local_pos.z_valid = true;
						local_pos.v_z_valid = true;
					}
				}
			}
		}
	}

	/* main loop */
	struct pollfd fds[1];

	fds[0].fd = vehicle_attitude_sub;
	fds[0].events = POLLIN;

	while (!_task_should_exit) {

		/* get actual thrust output */
		float thrust = armed.armed ? actuator.control[3] : 0.0f;

		int ret = poll(fds, 1, 20); // wait maximal 20 ms = 50 Hz minimum rate


		if (mavlink_fd < 0 && hrt_absolute_time() > mavlink_open_time) {
			/* try to reopen the mavlink log device with specified interval */
			mavlink_open_time = hrt_absolute_time() + mavlink_open_interval;
			mavlink_fd = open(MAVLINK_LOG_DEVICE, 0);
		}

		if (ret < 0) {
			/* poll error */
			mavlink_log_info(mavlink_fd, "[inav] poll error on init");
			continue;

		} else if (ret > 0) {
			/* act on attitude updates */
			loop_count++;

			t = hrt_absolute_time();
			dt = t_prev > 0 ? (t - t_prev) / 1000000.0f : 0.0f;
			dt = fmaxf(fminf(0.02, dt), 0.002);		// constrain dt from 2 to 20 ms
			t_prev = t;

			/* vehicle attitude */
			orb_copy(ORB_ID(vehicle_attitude), vehicle_attitude_sub, &att);
			attitude_updates++;

			bool updated;

			/* parameter update */
			orb_check(parameter_update_sub, &updated);
			if (updated) {
				struct parameter_update_s update;
				orb_copy(ORB_ID(parameter_update), parameter_update_sub, &update);
				parameters_update();
			}

			/* actuator */
			orb_check(actuator_sub, &updated);
			if (updated) {
				orb_copy(ORB_ID(actuator_controls_1), actuator_sub, &actuator);
			}

			/* armed */
			orb_check(armed_sub, &updated);
			if (updated) {
				orb_copy(ORB_ID(actuator_armed), armed_sub, &armed);
			}



			/* sensor combined */
			orb_check(sensor_combined_sub, &updated);
			if (updated) {
				orb_copy(ORB_ID(sensor_combined), sensor_combined_sub, &sensor);
				if (sensor.accelerometer_timestamp != accel_timestamp) {
					if (att.R_valid) {
						/* transform acceleration vector from body frame to NED frame */
						for (int i = 0; i < 3; i++) {
							acc[i] = 0.0f;

							for (int j = 0; j < 3; j++) {
								acc[i] += att.R[i][j] * sensor.accelerometer_m_s2[j];
							}
						}
						acc[2] += CONSTANTS_ONE_G;
					} else {
						memset(acc, 0, sizeof(acc));
					}
					accel_timestamp = sensor.accelerometer_timestamp;
					accel_updates++;

					if(can_estimate_xy)
						xy_predict(acc[0],acc[1]);

					if(can_estimate_z)
						z_predict(acc[2]);
				}
				if (sensor.baro_timestamp != baro_timestamp) {
					z_baro = -(sensor.baro_alt_meter - baro_offset);
//					z_baro_update(z_baro,params.r_z_baro);


					baro_timestamp = sensor.baro_timestamp;
					baro_updates++;
				}


			}

			baro_cur_reading_filtered = lowpass_filter(z_baro,pre_data,input_data,A,B);
			baro_velocity_filtered = (baro_cur_reading_filtered - baro_pre_reading_filtered)/dt;
			baro_pre_reading_filtered = baro_cur_reading_filtered;

			int est_baro_i = buf_ptr - 1 - math::min(EST_BUF_SIZE - 1, math::max(0, (int)(0.0f * 1000000.0f / PUB_INTERVAL)));
			if (est_baro_i < 0) {
				est_baro_i += EST_BUF_SIZE;
			}

			float corr_baro_vel = baro_velocity_filtered    - est_buf[est_baro_i][2][1];
			float corr_baro     = baro_cur_reading_filtered - est_buf[est_baro_i][2][0];

			int use_baro_vel = 1; // 1: use baro for velocity update; others: use baro for position update
			if(params.use_baro == 1) {
				if (use_baro_vel==1)
					z_vel_update(corr_baro_vel,params.r_z_baro); // 1: use baro for velocity update;
				else
					z_pos_update(corr_baro,params.r_z_baro);     // others: use baro for position update
			}




//			/* home position */
//			orb_check(home_position_sub, &updated);
//			if (updated) {
//				orb_copy(ORB_ID(home_position), home_position_sub, &home);
//				if (home.timestamp != home_timestamp) {
//					home_timestamp = home.timestamp;
//					double est_lat, est_lon;
//					float est_alt;
//					if (gps_ref_inited) {
//						/* calculate current estimated position in global frame */
//						est_alt = local_pos.ref_alt - local_pos.z;
//						map_projection_reproject(&gps_ref, local_pos.x, local_pos.y, &est_lat, &est_lon);
//					}
//					/* update reference */
//					map_projection_init(&gps_ref, home.lat, home.lon);
//
//					local_pos.ref_lat = home.lat;
//					local_pos.ref_lon = home.lon;
//					local_pos.ref_alt = home.alt;
//					local_pos.ref_timestamp = home.timestamp;
//
//					if (gps_ref_inited) {
//						/* reproject position estimate with new reference */
//						map_projection_project(&gps_ref, est_lat, est_lon, &_Xx(0), &_Xy(0));
//						_Xz(0) = -(est_alt - local_pos.ref_alt);
//					}
//					gps_ref_inited = true;
//				}
//			}

			/*vehicle nuc position estimate*/
			// "laser_position_estimate" is used for the measurement from NUC, which may be obtained by vision or UWB
			orb_check(laser_position_estimate_sub, &updated);
			if (updated) {
				orb_copy(ORB_ID(laser_position_estimate), laser_position_estimate_sub, &laser_position_estimate);
				if(params.att_corr_nuc_z==1)
					laser_position_estimate.z  = laser_position_estimate.z * att.R[2][2];

				if(nuc_valid == false)
				{
						if(laser_position_estimate.xy_valid){
							_Xx(0) = laser_position_estimate.x;
							_Xy(0) = laser_position_estimate.y;
							_Xx(1) = 0;
							_Xy(1) = 0;
							xy_ref_inited = true;

						}

						if(laser_position_estimate.v_xy_valid){
							_Xx(0) = 0;
							_Xy(0) = 0;
							_Xx(1) = laser_position_estimate.vx;
							_Xy(1) = laser_position_estimate.vy;
							xy_ref_inited = true;
						}

						if (laser_position_estimate.z_valid){
							z_init(laser_position_estimate.z,0);
							z_ref_inited = true;
						}


						if(laser_position_estimate.v_z_valid)	{
							z_init(0,laser_position_estimate.vz);
							z_ref_inited = true;
						}

						nuc_valid = true;
						warnx("NUC estimate valid");
						mavlink_log_info(mavlink_fd, "[inav] NUC estimate valid");
				}

				/* calculate index of estimated values in buffer */
				int est_i = buf_ptr - 1 - math::min(EST_BUF_SIZE - 1, math::max(0, (int)(params.delay_mea * 1000000.0f / PUB_INTERVAL)));
				if (est_i < 0) {
					est_i += EST_BUF_SIZE;
				}

				/* calculate correction for position */
				if(laser_position_estimate.xy_valid){

					corr_nuc[0][0] = laser_position_estimate.x - est_buf[est_i][0][0];
					corr_nuc[1][0] = laser_position_estimate.y - est_buf[est_i][1][0];

//					printf("x est is %8.4lf y est is %8.4lf\n",(double)laser_position_estimate.x,(double)laser_position_estimate.y);
					xy_pos_update(corr_nuc[0][0],corr_nuc[1][0],params.r_xy_nuc_p);
				}

				if(laser_position_estimate.z_valid  ){

							if(z_ref_inited && !use_nuc_z_pre && use_nuc_z)
							{
								surface_offset = _Xz(0) - laser_position_estimate.z;
							}

							corr_nuc[2][0] = laser_position_estimate.z + surface_offset - est_buf[est_i][2][0];

							use_nuc_z_pre = use_nuc_z;


							if (fabsf(corr_nuc[2][0]) > params.nuc_err) {
							/*correction is too large: spike or new ground level?*/
								if (corr_nuc_outrange_count < 3)
								{
									/* spike detected, ignore*/
									corr_nuc[2][0] = 0.0f;
									corr_nuc_outrange_count++;
									nuc_height_valid = false;
								}
								else
								{
									/* new ground level*/
									surface_offset = _Xz(0) - laser_position_estimate.z;
	//									surface_offset_rate = 0.0f;
									corr_nuc[2][0] = 0.0f;
									corr_nuc_outrange_count = 0;
									nuc_height_valid = true;
									local_pos.surface_bottom_timestamp = t;
									mavlink_log_info(mavlink_fd, "[inav] new surface level: %.2f", (double)surface_offset);
//									printf("[inav] new surface level: %8.4lf\n",(double)surface_offset);
								}
							}
							else {	//correction is ok, use it
								corr_nuc_outrange_count = 0;
								nuc_height_valid = true;

							}

					}

				if(nuc_height_valid == true && use_nuc_z)
				{
					z_pos_update(corr_nuc[2][0],params.r_z_nuc_p);
				}

				/* calculate correction for velocity */

				if(laser_position_estimate.v_xy_valid || laser_position_estimate.v_z_valid)
				{
					float R_vision[3][3];
					float vel_ned[3];
					float vel_body[3] = {laser_position_estimate.vx,laser_position_estimate.vy,laser_position_estimate.vz};
					/* save rotation matrix at this previous moment */
					memcpy(R_vision, R_buf[est_i], sizeof(R_vision));

					/* transform acceleration vector from body frame to NED frame */
					for (int i = 0; i < 3; i++) {
						vel_ned[i] = 0.0f;
						for (int j = 0; j < 3; j++) {
							vel_ned[i] += R_vision[i][j] * vel_body[j];
						}
					}



//					if(loop_count % 200 == 0)
//						printf("vel_ned is %8.4lf %8.4lf %8.4lf\n",(double)vel_ned[0],(double)vel_ned[1],(double)vel_ned[2]);

					if(laser_position_estimate.v_xy_valid){
						corr_nuc[0][1] = vel_ned[0] - est_buf[est_i][0][1];
						corr_nuc[1][1] = vel_ned[1] - est_buf[est_i][1][1];
						xy_vel_update(corr_nuc[0][1],corr_nuc[1][1],params.r_xy_nuc_v);
					}


					if(laser_position_estimate.v_z_valid && use_nuc_z)
					{
						corr_nuc[2][1] = vel_ned[2] - est_buf[est_i][2][1];
						z_vel_update(corr_nuc[2][1],params.r_z_nuc_v);
					}
				}

			}


			orb_check(manual_control_setpoint_sub,&updated);

			if(updated)
			{
				orb_copy(ORB_ID(manual_control_setpoint),manual_control_setpoint_sub,&man_sp);
			}


			orb_check(distance_sensor_sub, &updated);
			//updated = false;  // disable laser range finder for z update by Lin Feng on 28 June 2018

				if (updated)
				{
					orb_copy(ORB_ID(sensor_range_finder), distance_sensor_sub, &distance_z);


					if (distance_z.distance > 0.04f
						&& distance_z.distance < 125.0f
						&& (att.R[2][2] > 0.7f)){
//				if(true)
//				{
//					if(man_sp.aux1 > 0.7f)
//					{
//						distance_z.current_distance = man_sp.aux4;

						float height_distance = - distance_z.distance *att.R[2][2];
						distance_sensor_time = t;

						use_distance_z_prev = use_distance_z;

						if (!z_ref_inited)
						{
							z_init(height_distance,0);
							z_ref_inited = true;
						}

						if(z_ref_inited && !use_distance_z_prev && use_distance_z)
						{
							surface_offset = _Xz(0) - height_distance;
						}

							corr_distance = height_distance + surface_offset - _Xz(0);


//							printf("\n\n\n ######## \n");
//							printf("height dist is %8.4lf\n",(double)height_distance);
//							printf("corr_distance is %8.4lf\n",(double)corr_distance);
//							printf("corr_distance filtered is %8.4lf\n",(double)corr_distance_filtered);
//							printf("z est[0] is %8.4lf\n",(double)_Xz(0));
//							printf("baro alt is %8.4lf\n",(double)sensor.baro_alt_meter);
//							printf("baro offset is %8.4lf\n",(double)baro_offset);
//							printf("corr baro is %8.4lf\n",(double)corr_baro);
//							printf("surface_offset is %8.4lf\n",(double)surface_offset);
//							printf("distance_height_valid is %d\n",distance_height_valid);

							if (fabsf(corr_distance) > params.distance_err)
							{
								/*correction is too large: spike or new ground level?*/
								if (corr_distance_outrange_count < 20)
								{
									/* spike detected, ignore*/
									corr_distance = 0.0f;
									corr_distance_outrange_count++;
									distance_height_valid = false;
								}
								else
								{
									/* new ground level*/
									surface_offset = _Xz(0) - height_distance;
//									surface_offset_rate = 0.0f;
									corr_distance = 0.0f;
									corr_distance_outrange_count = 0;
									distance_height_valid = true;
									local_pos.surface_bottom_timestamp = t;
									mavlink_log_info(mavlink_fd, "[inav] new surface level: %.2f", (double)surface_offset);
//									printf("[inav] new surface level: %8.4lf\n",(double)surface_offset);
								}
							}
							else
							{
								distance_height_valid = true;
								distance_sensor_valid_time = t;
								corr_distance_outrange_count = 0;
							}


							// update the z using the velocity from the laser range finder.
							// update is only allowed when range measurement is larger than 0,
							// sometime the range measurement may be negative. height_distance = - laser_range
							if (params.use_distance_vel == 1 && height_distance<=0) {

								dist_t 	= hrt_absolute_time();
								dist_dt = t_prev > 0 ? (dist_t - dist_t_prev) / 1000000.0f : 0.0f;
								dist_dt = fmaxf(fminf(0.1, dist_dt), 0.002);		// constrain dt from 2ms to 100 ms
								dist_t_prev = dist_t;


								distance_cur_reading_filtered 	= lowpass_filter(height_distance, distance_pre_data, distance_input_data,A,B);

								// If fabs(height_distance) is less than 0.2 m, use filtered measurement since noise is significant.
								// If fabs(height_distance) is larger than 0.2 m, the raw measurement is good and used directly to avoid the delay.
								// The low pass filter needs to be running during the whole flight to keep filtered measurement smooth

								//if ( fabsf( height_distance ) >= 0.2f) {
								//	distance_cur_reading_filtered   = height_distance;
								//}

								distance_cur_reading_filtered   = height_distance;

								// Initialize "distance_pre_reading_filtered"
								if ( fabsf(distance_pre_reading_filtered) < 0.01f ) {
									distance_pre_reading_filtered = distance_cur_reading_filtered;
								}

								// compute the velocity
								distance_velocity_filtered = (distance_cur_reading_filtered - distance_pre_reading_filtered)/dist_dt;

								// If fabs(height_distance) is less than 0.15 m and computed velocity is less than 0.3 m/s,
								// we set velocity to 0 to reduce the drift on the ground.
								// Otherwise we will use the calculated velocity;
								//if ( fabsf( height_distance ) < 0.15f && fabsf(distance_velocity_filtered) < 0.3f) {
								if ( fabsf( height_distance ) < 0.15f && thrust < params.in_air_thr ) {
									//distance_velocity_filtered = 0.0f;

									//---Special case, for D-Drone project, update position using laser range directly when drone close to ground
									corr_distance = height_distance - _Xz(0);
									z_pos_update(corr_distance,params.r_z_distance_p);
								}

								// update the previous reading
								distance_pre_reading_filtered 	= distance_cur_reading_filtered;

								//printf("[inav] dist_vel = %8.4lf \n", (double) distance_velocity_filtered);

								int est_distance_i = buf_ptr - 1 - math::min(EST_BUF_SIZE - 1, math::max(0, (int)(0.0f * 1000000.0f / PUB_INTERVAL)));
								if (est_distance_i < 0) {
									est_distance_i += EST_BUF_SIZE;
								}


								float corr_distance_vel = distance_velocity_filtered - est_buf[est_distance_i][2][1];
								//printf("[inav] corr_dist_vel = %8.4lf \n", (double) corr_distance_vel);


								if (  fabs(distance_velocity_filtered) < 3.0 )
									z_vel_update(corr_distance_vel,params.r_z_distance_vel);

							}

						}
						else
						{
							distance_height_valid = false;
						}

					if(distance_height_valid == true && use_distance_z)
						z_pos_update(corr_distance,params.r_z_distance_p);


				}








			/* vehicle GPS position */
			orb_check(vehicle_gps_position_sub, &updated);  // use GPS
			updated = false;  // disable the usage of GPS
			if (updated) {
				orb_copy(ORB_ID(vehicle_gps_position), vehicle_gps_position_sub, &gps);

				bool reset_est = false;

				/* hysteresis for GPS quality */
				if (gps_valid) {
					if (gps.eph > max_eph_epv || gps.epv > max_eph_epv || gps.fix_type < 3) {
						gps_valid = false;
						mavlink_log_info(mavlink_fd, "[inav] GPS signal lost");
					}

				} else {
					if (gps.eph < max_eph_epv * 0.2f && gps.epv < max_eph_epv * 0.2f && gps.fix_type >= 3) {
						gps_valid = true;
						reset_est = true;
						mavlink_log_info(mavlink_fd, "[inav] GPS signal found");
					}
				}

				if (gps_valid) {
					double lat = gps.lat * 1e-7;
					double lon = gps.lon * 1e-7;
					float alt =  gps.alt * 1e-3;

					/* initialize reference position if needed */
					if (!gps_ref_inited) {
						if (ref_init_start == 0) {
							ref_init_start = t;

						} else if (t > ref_init_start + ref_init_delay) {
							gps_ref_inited = true;

							/* set position estimate to (0, 0, 0), use GPS velocity for XY */
							if(!xy_ref_inited){
								_Xx(0) = 0.0f;
								_Xx(1) = gps.vel_n_m_s;
								_Xy(0) = 0.0f;
								_Xy(1) = gps.vel_e_m_s;

								local_pos.ref_lat = lat;
								local_pos.ref_lon = lon;
								map_projection_init(&gps_ref, lat, lon);
								xy_ref_inited = true;
							}
							else /* xy position is inited by some other sources, set the ref lat lon to be consistant with the external source*/
							{
								struct map_projection_reference_s temp_gps_ref;
								map_projection_init(&temp_gps_ref, lat, lon);
								double lat_ref, lon_ref;
								// get the reference lat lon by reproject the x,y based on current lat, lon
								map_projection_reproject(&temp_gps_ref,-_Xx(0),-_Xx(1),&lat_ref,&lon_ref);

								// Set the reference coordinate
								map_projection_init(&gps_ref,lat_ref,lon_ref);
								local_pos.ref_lat = lat_ref;
								local_pos.ref_lon = lon_ref;
							}


							if(!z_ref_inited){
								z_init(0,gps.vel_d_m_s);
								z_ref_inited = true;
								local_pos.ref_alt = alt;
								local_pos.ref_timestamp = t;

							}
							else  //z is inited by some external sources, set the gps ref alt consistant z = alt - ref_alt
 							{
								local_pos.ref_alt = alt + _Xz(0);
								local_pos.ref_timestamp = t;
							}


							warnx("init ref: lat=%.7f, lon=%.7f, alt=%.2f", (double)lat, (double)lon, (double)alt);
							mavlink_log_info(mavlink_fd, "[inav] init ref: %.7f, %.7f, %.2f", (double)lat, (double)lon, (double)alt);
						}
					}

					if (gps_ref_inited) {
						/* project GPS lat lon to plane */
						float gps_proj[2];
						map_projection_project(&gps_ref, lat, lon, &(gps_proj[0]), &(gps_proj[1]));

						/* reset position estimate when GPS becomes good I think this one is not necessary here */
						if (reset_est ) {
							if(!external_pos_source_valid)
							{
								_Xx(0) = gps_proj[0];
								_Xx(1) = gps.vel_n_m_s;
								_Xy(0) = gps_proj[1];
								_Xy(1) = gps.vel_e_m_s;
							}
							else
							{
								struct map_projection_reference_s temp_gps_ref;
								map_projection_init(&temp_gps_ref, lat, lon);
								double lat_ref, lon_ref;
								// get the reference lat lon by reproject the x,y based on current lat, lon
								map_projection_reproject(&temp_gps_ref,-_Xx(0),-_Xy(0),&lat_ref,&lon_ref);

								// Set the reference coordinate
								map_projection_init(&gps_ref,lat_ref,lon_ref);
								local_pos.ref_lat = lat_ref;
								local_pos.ref_lon = lon_ref;
								map_projection_project(&gps_ref, lat, lon, &(gps_proj[0]), &(gps_proj[1]));
							}
							local_pos.ref_alt = alt + _Xz(0);
							local_pos.ref_timestamp = t;
						}

						/* calculate index of estimated values in buffer */
						int est_i = buf_ptr - 1 - math::min(EST_BUF_SIZE - 1, math::max(0, (int)(params.delay_gps * 1000000.0f / PUB_INTERVAL)));
						if (est_i < 0) {
							est_i += EST_BUF_SIZE;
						}

						/* save rotation matrix at this moment */
						memcpy(R_gps, R_buf[est_i], sizeof(R_gps));
						memcpy(angular_speed_gps,angular_speed_buf[est_i],sizeof(angular_speed_gps));

						/* transform from body frame into ned frame */
						for (int i = 0; i < 3; i++) {
								float c = 0.0f;

								for (int j = 0; j < 3; j++) {
									c += R_gps[i][j] * params.gps_antenna_offset[j];
								}

								if (isfinite(c)) {
									gps_ant_offset_ned[i] = c;
								}
							}

						/* calculate correction for position */
						if(!reset_est){
						corr_gps[0][0] = gps_proj[0] - gps_ant_offset_ned[0] - est_buf[est_i][0][0];
						corr_gps[1][0] = gps_proj[1] - gps_ant_offset_ned[1] - est_buf[est_i][1][0];
						corr_gps[2][0] = local_pos.ref_alt - alt - gps_ant_offset_ned[2] - est_buf[est_i][2][0] + gps_offset;
						}
						/* calculate correction for velocity */
						if (gps.vel_ned_valid /*&& !reset_est*/) {

							gps_rotation_vel[0] =  angular_speed_gps[1] *  params.gps_antenna_offset[2] - angular_speed_gps[2] * params.gps_antenna_offset[1];
							gps_rotation_vel[1] = -angular_speed_gps[0] *  params.gps_antenna_offset[2] + angular_speed_gps[2] * params.gps_antenna_offset[0];
							gps_rotation_vel[2] =  angular_speed_gps[0] *  params.gps_antenna_offset[1] - angular_speed_gps[1] * params.gps_antenna_offset[0];

							for (int i = 0; i < 3; i++) {
								float c = 0.0f;

								for (int j = 0; j < 3; j++) {
									c += R_gps[i][j] * gps_rotation_vel[j];
								}

								if (isfinite(c)) {
									gps_rotation_vel_ned[i] = c;
								}
							}
							corr_gps[0][1] = gps.vel_n_m_s - gps_rotation_vel_ned[0] - est_buf[est_i][0][1];
							corr_gps[1][1] = gps.vel_e_m_s - gps_rotation_vel_ned[1] - est_buf[est_i][1][1];
							corr_gps[2][1] = gps.vel_d_m_s/* - gps_rotation_vel_ned[2]*/ - est_buf[est_i][2][1]; // No need to compensate the z direction

						} else {
							corr_gps[0][1] = 0.0f;
							corr_gps[1][1] = 0.0f;
							corr_gps[2][1] = 0.0f;
						}

						// scale for the gps measurement noise
						w_gps_xy = fmaxf(min_eph_epv, gps.eph)/ min_eph_epv ;
						w_gps_z  = fmaxf(min_eph_epv, gps.epv) /min_eph_epv ;

						xy_pos_update(corr_gps[0][0],corr_gps[1][0],params.r_xy_gps_p * w_gps_xy * w_gps_xy);
						xy_vel_update(corr_gps[0][1],corr_gps[1][1],params.r_xy_gps_v * w_gps_xy * w_gps_xy);

						bool b_use_gps_z = false; // added by Lin Feng on 31 May 2018 to test use baro for altitude hold
						if (b_use_gps_z) {
							z_pos_update(corr_gps[2][0],params.r_z_gps_p * w_gps_z * w_gps_z);
							z_vel_update(corr_gps[2][1],params.r_z_gps_v * w_gps_z * w_gps_z);
						}

					}

				} else {
					/* no GPS lock */
					memset(corr_gps, 0, sizeof(corr_gps));
					ref_init_start = 0;
				}
				gps_updates++;
			}
		}


		/* check for timeout on GPS topic */
		if (gps_valid && (t > (gps.timestamp_position + gps_topic_timeout))) {
			gps_valid = false;
			warnx("GPS timeout");
			mavlink_log_info(mavlink_fd, "[inav] GPS timeout");
		}

		/* check for lidar measurement timeout */
		if (distance_height_valid && (t > (distance_sensor_time + distance_sensor_timeout))) {
			distance_height_valid = false;
			warnx("LIDAR timeout");
			mavlink_log_info(mavlink_fd, "[inav] LIDAR timeout");
		}


		/* check for nuc measurement timeout */
		if (nuc_valid&&(t > (laser_position_estimate.timestamp + nuc_topic_timeout))) {
			nuc_valid = false;
			warnx("NUC timeout");
			mavlink_log_info(mavlink_fd, "[inav] NUC timeout");
		}




		/* increase EPH/EPV on each step */
		if (eph < max_eph_epv) {
			eph *= 1.0f + dt;
		}
		if (epv < max_eph_epv) {
			epv += 0.005f * dt;	// add 1m to EPV each 200s (baro drift)
		}

		/* use GPS if it's valid and reference position initialized */
		bool use_gps_xy = gps_ref_inited && gps_valid && params.r_xy_gps_p > MIN_VALID_R;
		bool use_gps_z = gps_ref_inited && gps_valid && params.r_z_gps_p > MIN_VALID_R;

		bool use_nuc_xy = nuc_valid && params.r_xy_nuc_p > MIN_VALID_R;
	    use_nuc_z = nuc_valid && (nuc_height_valid || laser_position_estimate.v_z_valid) && params.use_nuc_z && params.r_z_nuc_p > MIN_VALID_R;
		use_distance_z = distance_height_valid && params.r_z_distance_p > MIN_VALID_R && params.use_distance;

		if(use_nuc_xy)
		{
			eph = min_eph_epv;
		}

		if(use_gps_xy)
		{
			eph = gps.eph;
		}

		if(use_gps_z)
		{
			eph = gps.epv;
		}

		if(use_nuc_z || use_distance_z)
		{
			epv = min_eph_epv;
		}




		external_pos_source_valid = use_nuc_xy;

		if(use_gps_z || use_distance_z || use_nuc_z)
			{
				external_distance_valid = true;
				external_distance_valid_time = t;
			}
		else
			{
				//give some time for the external distance valid to be true so that the height filter and bias
				//estimation is not seriously affected
				if(t < external_distance_valid_time + external_distance_sensor_timeout)
				{
					external_distance_valid = false;
				}
			}

		can_estimate_xy = use_gps_xy || use_nuc_xy;
		can_estimate_z = params.use_baro || use_nuc_z || use_distance_z || use_gps_z;

		bool dist_bottom_valid = (t < distance_sensor_valid_time + distance_sensor_valid_timeout);




		/* detect land */
		alt_avg += (- _Xz(0) - alt_avg) * dt / params.land_t;
		float alt_disp2 = - _Xz(0) - alt_avg;
		alt_disp2 = alt_disp2 * alt_disp2;
		float land_disp2 = params.land_disp * params.land_disp;
		///* get actual thrust output */
		//float thrust = armed.armed ? actuator.control[3] : 0.0f;


		if (landed) {
			if(thrust >= params.in_air_thr){ //kangli
				landed = false;
				landed_time = 0;
			}
		} else {
			if (alt_disp2 < land_disp2 && thrust < params.land_thr) {
				if (landed_time == 0) {
					landed_time = t;    // land detected first time

				} else {
					if (t > landed_time + params.land_t * 1000000.0f) {
						landed = true;
						landed_time = 0;
					}
				}

			} else {
				landed_time = 0;
			}
		}

		if (_verbose_mode) {
			/* print updates rate */
			if (t > updates_counter_start + updates_counter_len) {
				float updates_dt = (t - updates_counter_start) * 0.000001f;
				warnx(
					"updates rate: accelerometer = %.1f/s, baro = %.1f/s, gps = %.1f/s, attitude = %.1f/s",
					(double)(accel_updates / updates_dt),
					(double)(baro_updates / updates_dt),
					(double)(gps_updates / updates_dt),
					(double)(attitude_updates / updates_dt));
				updates_counter_start = t;
				accel_updates = 0;
				baro_updates = 0;
				gps_updates = 0;
				attitude_updates = 0;
			}
		}


//		if(loop_count % 100 == 0)
//		{
//			printf("z est is ###\n");
//			_Xz.print();
//			printf("z Pz is ###\n");
//			_Pz.print();
//		}


		if (t > pub_last + PUB_INTERVAL) {
			pub_last = t;

			/* push current estimate to buffer */
			est_buf[buf_ptr][0][0] = _Xx(0);
			est_buf[buf_ptr][0][1] = _Xx(1);
			est_buf[buf_ptr][1][0] = _Xy(0);
			est_buf[buf_ptr][1][1] = _Xy(1);
			est_buf[buf_ptr][2][0] = _Xz(0);
			est_buf[buf_ptr][2][1] = _Xz(1);

			/* push current rotation matrix to buffer */
			memcpy(R_buf[buf_ptr], att.R, sizeof(att.R));
			angular_speed_buf[buf_ptr][0] = att.rollspeed;
			angular_speed_buf[buf_ptr][1] = att.pitchspeed;
			angular_speed_buf[buf_ptr][2] = att.yawspeed;

			buf_ptr++;
			if (buf_ptr >= EST_BUF_SIZE) {
				buf_ptr = 0;
			}

			/* publish local position */
			local_pos.xy_valid = can_estimate_xy;
			local_pos.v_xy_valid = can_estimate_xy;
			local_pos.xy_global = local_pos.xy_valid && use_gps_xy;
			local_pos.z_global = local_pos.z_valid && use_gps_z;

			if(	isfinite(_Xx(0)) && isfinite(_Xx(1)) && isfinite(_Xy(0)) && isfinite(_Xy(1)) ){

				local_pos.x = _Xx(0);
				local_pos.vx =_Xx(1);
				local_pos.y = _Xy(0);
				local_pos.vy = _Xy(1);
				local_pos.xy_nan = false;
// 				temp for debug purpose



			}
			else{
				local_pos.xy_nan = true;
				if(loop_count % 100 == 0){
					mavlink_log_critical(mavlink_fd, "[inav]Local XY NaN");
				}
			}

			if(	isfinite(_Xz(0)) && isfinite(_Xz(1)) ){
//				local_pos.z = _Xz(0);
//				local_pos.vz = laser_position_estimate.vz;
//				local_pos.z_nan = false;

				local_pos.z = _Xz(0);
				local_pos.vz = _Xz(1);
				local_pos.z_nan = false;
			}
			else{
				local_pos.z_nan = true;
				if(loop_count % 100 == 0){
					mavlink_log_critical(mavlink_fd, "[inav]Local Z NaN");
				}
			}

			if(local_pos.xy_nan || local_pos.z_nan){
				local_pos.xy_valid = false;
				local_pos.z_valid = false;
				local_pos.v_xy_valid = false;
				local_pos.v_z_valid = false;
				local_pos.xy_global  = false;
				local_pos.z_global = false;
				local_pos.pos_nan = true;
			}
			//not reset to false here, once local pos NaN happen, keep alarm.

			local_pos.landed = landed;
			local_pos.yaw = att.yaw;
			local_pos.dist_bottom_valid = dist_bottom_valid;
			local_pos.eph = eph;
			local_pos.epv = epv;

			if (local_pos.dist_bottom_valid) {
				local_pos.dist_bottom = distance_z.distance/*-_Xz(0) - surface_offset*/;
				local_pos.dist_bottom_rate = 0/*corr_distance - corr_distance_filtered*/;
			}

			local_pos.timestamp = t;

			orb_publish(ORB_ID(vehicle_local_position), vehicle_local_position_pub, &local_pos);

			if (local_pos.xy_global && local_pos.z_global && !local_pos.pos_nan) {
				/* publish global position */
				global_pos.timestamp = t;
				global_pos.time_gps_usec = gps.time_gps_usec;

				double est_lat, est_lon;
				map_projection_reproject(&gps_ref, local_pos.x, local_pos.y, &est_lat, &est_lon);

				global_pos.lat = est_lat;
				global_pos.lon = est_lon;
				global_pos.alt = local_pos.ref_alt - local_pos.z;

				global_pos.vel_n = local_pos.vx;
				global_pos.vel_e = local_pos.vy;
				global_pos.vel_d = local_pos.vz;

				global_pos.yaw = local_pos.yaw;

				global_pos.eph = eph;
				global_pos.epv = epv;

				if (vehicle_global_position_pub < 0) {
					vehicle_global_position_pub = orb_advertise(ORB_ID(vehicle_global_position), &global_pos);

				} else {
					orb_publish(ORB_ID(vehicle_global_position), vehicle_global_position_pub, &global_pos);
				}
			}
		}
	}

	warnx("stopped");
	mavlink_log_info(mavlink_fd, "[inav] stopped");
}

int position_estimator_inav_main(int argc, char *argv[])
{
	if (argc < 1) {
		errx(1, "usage: mc_pos_control {start|stop|status}");
	}

	if (!strcmp(argv[1], "start")) {
		if (Position_estimate::pos_estimate != nullptr) {
			warnx("already running");
			/* this is not an error */
			exit(0);
		}

		Position_estimate::pos_estimate = new PositionEstimatorInav;



		if (Position_estimate::pos_estimate == nullptr) {
			errx(1, "alloc failed");
		}

		if (OK != Position_estimate::pos_estimate->start()) {
			delete Position_estimate::pos_estimate;
			Position_estimate::pos_estimate = nullptr;
			err(1, "start failed");
		}

//		_task_should_exit = false;
//		position_estimator_inav_task = task_spawn_cmd("position_estimator_inav",
//					       SCHED_DEFAULT, SCHED_PRIORITY_MAX - 5, 5500,
//					       position_estimator_inav_thread_main,
//					       (argv) ? (const char **) &argv[2] : (const char **) NULL);
		exit(0);
	}

	if (!strcmp(argv[1], "stop")) {
		if (Position_estimate::pos_estimate == nullptr) {
			warnx("not running");
			/* this is not an error */
			exit(0);
		} else {
			delete Position_estimate::pos_estimate;
			Position_estimate::pos_estimate = nullptr;
			exit(0);
		}

		exit(0);
	}

	if (!strcmp(argv[1], "status")) {
		if (Position_estimate::pos_estimate) {
			warnx("app is running");

		} else {
			warnx("app not started");
		}

		exit(0);
	}

	warnx("unrecognized command");
	exit(1);
}


int PositionEstimatorInav::start()
{

	ASSERT(_position_estimator_inav_task == -1);
	_task_should_exit = false;
	_position_estimator_inav_task = task_spawn_cmd("position_estimator_inav",
					   SCHED_DEFAULT, SCHED_PRIORITY_MAX - 5, 5500,
					   (main_t)&PositionEstimatorInav::task_main_trampoline,
					   nullptr);

	if (_position_estimator_inav_task < 0) {
		warn("task start failed");
		return -1;
	}

	return OK;
}
