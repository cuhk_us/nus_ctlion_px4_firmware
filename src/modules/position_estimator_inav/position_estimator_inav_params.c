/****************************************************************************
 *
 *   Copyright (c) 2013, 2014 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <systemlib/param/param.h>
/*
 * @file position_estimator_inav_params.c
 *
 * @author Anton Babushkin <rk3dov@gmail.com>
 *
 * Parameters for position_estimator_inav
 */
/**
 * Z axis weight for barometer
 *
 * Weight (cutoff frequency) for barometer altitude measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_Z_BARO, 0.5f);

PARAM_DEFINE_FLOAT(INAV_TAO_BARO, 5.0f);

PARAM_DEFINE_FLOAT(INAV_BARO_BETA,0.6f);



/**
 * Z axis weight for GPS
 *
 * Weight (cutoff frequency) for GPS altitude measurements. GPS altitude data is very noisy and should be used only as slow correction for baro offset.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_Z_GPS_P, 1.0f);

PARAM_DEFINE_FLOAT(INAV_R_Z_GPS_V, 0.2f);

/**
 * Z axis weight for vision
 *
 * Weight (cutoff frequency) for vision altitude measurements. vision altitude data is very noisy and should be used only as slow correction for baro offset.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_Z_VIS_P, 0.5f);
/**
 * Z axis weight for slam measurement
 *
 * Weight (cutoff frequency) for vision altitude measurements. vision altitude data is very noisy and should be used only as slow correction for baro offset.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_Z_MEA_P, 0.01f);


PARAM_DEFINE_FLOAT(INAV_R_Z_MEA_V,1.0f);

/**
 * XY axis weight for GPS position
 *
 * Weight (cutoff frequency) for GPS position measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_GPS_P, 1.0f);

/**
 * XY axis weight for GPS velocity
 *
 * Weight (cutoff frequency) for GPS velocity measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_GPS_V, 0.1f);

/**
 * XY axis weight for vision position
 *
 * Weight (cutoff frequency) for vision position measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_VIS_P, 5.0f);

/**
 * XY axis weight for slam measurement position
 *
 * Weight (cutoff frequency) for slam measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_MEA_P, 0.01f);

PARAM_DEFINE_FLOAT(INAV_R_XY_MEA_V, 0.1f);

/**
 * XY axis weight for vision velocity
 *
 * Weight (cutoff frequency) for vision velocity measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_VIS_V, 0.0f);

/**
 * XY axis weight for optical flow
 *
 * Weight (cutoff frequency) for optical flow (velocity) measurements.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_FLOW, 5.0f);

/**
 * XY axis weight for resetting velocity
 *
 * When velocity sources lost slowly decrease estimated horizontal velocity with this weight.
 *
 * @min 0.0
 * @max 10.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_XY_RES_V, 0.5f);

/**
 * XY axis weight factor for GPS when optical flow available
 *
 * When optical flow data available, multiply GPS weights (for position and velocity) by this factor.
 *
 * @min 0.0
 * @max 1.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_R_GPS_FLOW, 0.1f);

/**
 * Accelerometer bias estimation weight
 *
 * Weight (cutoff frequency) for accelerometer bias estimation. 0 to disable.
 *
 * @min 0.0
 * @max 0.1
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_Q_ACC_BIAS, 0.000001f);
PARAM_DEFINE_FLOAT(INAV_Q_ACC,0.09f);

/**
 * Optical flow scale factor
 *
 * Factor to convert raw optical flow (in pixels) to radians [rad/px].
 *
 * @min 0.0
 * @max 1.0
 * @unit rad/px
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_FLOW_K, 0.15f);

/**
 * Minimal acceptable optical flow quality
 *
 * 0 - lowest quality, 1 - best quality.
 *
 * @min 0.0
 * @max 1.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_FLOW_Q_MIN, 0.5f);

/**
 * Weight for sonar filter
 *
 * Sonar filter detects spikes on sonar measurements and used to detect new surface level.
 *
 * @min 0.0
 * @max 1.0
 * @group Position Estimator INAV
*/


PARAM_DEFINE_FLOAT(INAV_LASER_FILT,0.05f);

PARAM_DEFINE_FLOAT(INAV_LASER_ERR,0.5f);
/**
 * Land detector time
 *
 * Vehicle assumed landed if no altitude changes happened during this time on low throttle.
 *
 * @min 0.0
 * @max 10.0
 * @unit s
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_LAND_T, 3.0f);

/**
 * Land detector altitude dispersion threshold
 *
 * Dispersion threshold for triggering land detector.
 *
 * @min 0.0
 * @max 10.0
 * @unit m
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_LAND_DISP, 0.7f);

/**
 * Land detector throttle threshold
 *
 * Value should be lower than minimal hovering thrust. Half of it is good choice.
 *
 * @min 0.0
 * @max 1.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_LAND_THR, 0.15f);

PARAM_DEFINE_FLOAT(INAV_IN_AIR_THR,0.28f);

/**
 * GPS delay
 *
 * GPS delay compensation
 *
 * @min 0.0
 * @max 1.0
 * @unit s
 * @group Position Estimator INAV
 */
PARAM_DEFINE_FLOAT(INAV_DELAY_GPS, 0.2f);

PARAM_DEFINE_FLOAT(INAV_DELAY_MEA,0.2f);

PARAM_DEFINE_FLOAT(INAV_DIST_FILT, 0.05f);

PARAM_DEFINE_FLOAT(INAV_DIST_ERR, 0.5f);

PARAM_DEFINE_FLOAT(INAV_R_Z_DIST, 0.1f);


/**
 * Disable vision input
 *
 * Set to the appropriate key (328754) to disable vision input.
 *
 * @min 0.0
 * @max 1.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_INT32(CBRK_NO_VISION, 0);

PARAM_DEFINE_FLOAT(INAV_GPS_OFFX,0.0f);

PARAM_DEFINE_FLOAT(INAV_GPS_OFFY,0.0f);

PARAM_DEFINE_FLOAT(INAV_GPS_OFFZ,0.0f);

PARAM_DEFINE_INT32(INAV_USE_DIST,0);

PARAM_DEFINE_INT32(INAV_USE_NUCZ,0);

PARAM_DEFINE_INT32(NUC_ATT_CORR_Z,0);


PARAM_DEFINE_INT32(INAV_USE_BARO,1);

PARAM_DEFINE_FLOAT(INAV_CAMERA_TILT,0.349f);


/**
 * Z velocity estimation using laser distance sensor
 *
 * Value should be either 0 or 1.
 *
 * @min 0.0
 * @max 1.0
 * @group Position Estimator INAV
 */
PARAM_DEFINE_INT32(INAV_USE_DIST_V,0);

PARAM_DEFINE_FLOAT(INAV_R_Z_DIST_V, 0.5f);

