/*
 * stats.h
 *
 *  Created on: Oct 20, 2014
 *      Author: Administrator
 */

#ifndef STATS_H_
#define STATS_H_

#include <uORB/uORB.h>
#include <uORB/topics/sys_stats.h>
#include <uORB/topics/vehicle_status.h>

/*struct StatsInfo {
	float flight_min;  !< accumulated flight time in minutes
	float disk_usage;  !< micro SD card usage in percentage
};*/

class Stats {
public:
	Stats();
	~Stats();

	/*
	 * @brief Start the Stats command
	 * @return OK on success
	 */
	int	start();

	/**
	 * @brief Shim for calling task_main from task_create.
	 */
	static void	task_main_trampoline(int argc, char *argv[]);

	/**
	 * @brief Main task.
	 */
	void task_main();

	/*
	 * @brief Write accumulated running time and disk usage
	 * @param input statsInfo the data from dataman
	 * @param input passedTime accumulated usec since in the air
	 */
	void WriteStatsData(sys_stats_s statsInfo, uint64_t passedTime);

	/*
	 * @brief Read the stats info: accumulated time and disk usage
	 */
	void ReadStatsData(sys_stats_s &statsInfo) const;

	/**
	 * Check for changes in subscribed topics.
	 */
	void		poll_subscriptions();

private:
	bool m_task_should_exit;		/**< if true, stats task should exit */
	int m_stats_task;	/*!< handle of the stats task */
	sys_stats_s m_statsInfo; /*!< StatsInfo variable to hold the data item */
	uint64_t m_tOnboard;	/*!< Elapsed time since start time */
	uint64_t m_tFlight;		/*!< Flight time of usec in this time (since power on) till power off, accumulated in air time */
	orb_advert_t	m_stats_pub; ///< file handle for publishing topic
	int     _v_status_sub; 		/** < vehicle status subscription */
	struct vehicle_status_s     _v_status; ///< vehicle status, use in air to start timing
};

#endif /* STATS_H_ */
