/*
 * stats_main.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: Administrator
 */

#include <nuttx/config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <systemlib/err.h>
#include <systemlib/systemlib.h>
#include <drivers/drv_hrt.h>

#include <dataman/dataman.h>
#include "stats.h"

extern "C" __EXPORT int stats_main(int argc, char *argv[]);

static uint64_t start_time = 0;
static uint64_t time_inAir = 0;
static uint64_t start_time_onboard = 0;

namespace stats
{
	Stats *g_stats;
}

Stats::Stats() :
		m_task_should_exit(false),
		m_stats_task(-1),
		m_statsInfo{},
		m_tOnboard(0),
		m_tFlight(0),
		m_stats_pub(-1),
		_v_status_sub(-1),
		_v_status{}
{

}

Stats::~Stats()
{
	if (m_stats_task != -1) {
		m_task_should_exit = true;

		/* wait for a second for the task to quit at our request */
		unsigned i = 0;
		do {
			/* wait 20ms */
			usleep(20000);
			if (++i > 50) {
				task_delete(m_stats_task);
				break;
			}
		} while (m_stats_task != -1);
	}

	stats::g_stats = nullptr;
}

int Stats::start()
{
	ASSERT(m_stats_task == -1);

	// get the system start time
	start_time_onboard = start_time = hrt_absolute_time();

	m_stats_task = task_spawn_cmd("stats",
			SCHED_DEFAULT,
			SCHED_PRIORITY_MAX - 5,
			2000,
			(main_t)&Stats::task_main_trampoline,
			nullptr);

	if (m_stats_task <= 0) {
		warn("task start failed");
		return -errno;
	}

	return OK;
}

void Stats::task_main_trampoline(int argc, char *argv[])
{
	stats::g_stats->task_main();
}

void Stats::task_main()
{
	warnx("Initilizing stats task...\n");

	_v_status_sub  = orb_subscribe(ORB_ID(vehicle_status));
	poll_subscriptions();

	while (!m_task_should_exit) {
		sleep(5);
		poll_subscriptions();

//		uint32_t passedTime = 0;
//		_v_status.condition_landed = false;
		if (_v_status.condition_landed == true) {
			// ground status, reset timer
			start_time = hrt_absolute_time();
			time_inAir = 0;
		}
		else {
			// in air, start timing
			time_inAir = hrt_absolute_time() - start_time;
		}

		m_tOnboard = hrt_absolute_time() - start_time_onboard;
//		warnx("flight time short %.3f, this time in air %.3f, onboard time %.3f\n",
//				double (time_inAir / 1000000.0f / 60.0f), double (m_tFlight / 1000000.0f / 60.0f), double (m_tOnboard / 1000000.0f / 60.0f));
		// Read the record data first then write new values
//		struct StatsInfo statsInfo;
		ReadStatsData(m_statsInfo);
		WriteStatsData(m_statsInfo, time_inAir);

		m_statsInfo.flight_time = (uint16_t)(m_tFlight / 1000000.0f);
		m_statsInfo.onboard_time = (uint16_t)(m_tOnboard / 1000000.0f);

		if (m_stats_pub > 0) {
//			warnx("publish");
			orb_publish(ORB_ID(sys_stats), m_stats_pub, &m_statsInfo);
		}
		else {
			warnx("advertise");
			m_stats_pub = orb_advertise(ORB_ID(sys_stats), &m_statsInfo);
		}
	}

	warnx("existing");
	m_stats_task = -1;
	_exit(0);
}

void Stats::WriteStatsData(sys_stats_s statsInfo, uint64_t passedTime)
{
	float mins = (float)(passedTime) / 1000000.0f / 60.0f + statsInfo.total_flight_min ;

	statsInfo.disk_usage = 0.2;
	statsInfo.total_flight_min = mins;

	if ( dm_write(DM_KEY_STATSINFO, 0, DM_PERSIST_POWER_ON_RESET, &statsInfo, sizeof(sys_stats_s)) != sizeof(sys_stats_s) ) {
		warnx("[Stats] write error\n");
	}
	else {
		m_tFlight += passedTime;
		start_time = hrt_absolute_time();
		//warnx("[Stats] write finish\n");
	}
}

void Stats::ReadStatsData(sys_stats_s &statsInfo) const
{

	if (dm_read(DM_KEY_STATSINFO, 0, &statsInfo, sizeof(sys_stats_s)) != sizeof(sys_stats_s)) {
		warnx("[Stats] read error\n");
	}
	else {
		//warnx("[Stats] read finish, flight time %.3f\n", /*statsInfo2Read.disk_usage,*/ (double)statsInfo.total_flight_min);
	}
}

void Stats::poll_subscriptions()
{
	bool updated;

	orb_check(_v_status_sub,&updated);
	if(updated){
		orb_copy(ORB_ID(vehicle_status), _v_status_sub, &_v_status);
	}
}

static void usage()
{
	errx(1, "usage: stats {start|stop|status}");
}

int stats_main(int argc, char *argv[])
{
	if (argc < 2) {
		usage();
	}
	printf("[stats_main] stats starts\n");

	if (!strcmp(argv[1], "start")) {
		if (stats::g_stats != nullptr)	{
			errx(1, "Already running");
		}
		warnx("Allocate Stats");

		stats::g_stats = new Stats;

		if (stats::g_stats == nullptr) {
			errx(1, "alloc failed");
		}

		if (OK != stats::g_stats->start()) {
			delete stats::g_stats;
			stats::g_stats = nullptr;
			errx(1, "start failed");
		}

		return 0;
	}

	if (stats::g_stats == nullptr)
		errx(1, "not running");

	if (!strcmp(argv[1], "stop")) {
		delete stats::g_stats;
		stats::g_stats = nullptr;
	}
	else if (!strcmp(argv[1], "status")) {
		printf("[stats_main] status to be displayed\n");
//		stats::g_stats->status();
	}
	else {
		usage();
	}

	return 0;
}
