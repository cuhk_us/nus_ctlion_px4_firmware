#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include <drivers/drv_hrt.h>
#include <systemlib/err.h>
#include <fcntl.h>
#include <stdlib.h>
#include "lightware_uart_topic.h"
#include <string.h>

#include <systemlib/systemlib.h>

ORB_DEFINE(lightware_uart_altitude, struct lightware_uart_data_s);

static bool thread_should_exit = false;
static bool thread_running = false;
static int daemon_task;

__EXPORT int lightware_uart_main(int argc, char *argv[]);
int lightware_uart_thread_main(int argc, char *argv[]);




static int uart_init(const char * uart_name);
static int set_uart_baudrate(const int fd, unsigned int baud);
static void usage(const char *reason);


int set_uart_baudrate(const int fd, unsigned int baud)
{
    int speed;

    switch (baud) {
        case 9600:   speed = B9600;   break;
        case 19200:  speed = B19200;  break;
        case 38400:  speed = B38400;  break;
        case 57600:  speed = B57600;  break;
        case 115200: speed = B115200; break;
        default:
            warnx("ERR: baudrate: %d\n", baud);
            return -EINVAL;
    }

    struct termios uart_config;

    int termios_state;

    /* fill the struct for the new configuration */
    tcgetattr(fd, &uart_config);
    /* clear ONLCR flag (which appends a CR for every LF) */
    uart_config.c_oflag &= ~ONLCR;
    /* no parity, one stop bit */
    uart_config.c_cflag &= ~(CSTOPB | PARENB);
    /* set baud rate */
    if ((termios_state = cfsetispeed(&uart_config, speed)) < 0) {
        warnx("ERR: %d (cfsetispeed)\n", termios_state);
        return false;
    }

    if ((termios_state = cfsetospeed(&uart_config, speed)) < 0) {
        warnx("ERR: %d (cfsetospeed)\n", termios_state);
        return false;
    }

    if ((termios_state = tcsetattr(fd, TCSANOW, &uart_config)) < 0) {
        warnx("ERR: %d (tcsetattr)\n", termios_state);
        return false;
    }

    return true;
}


int uart_init(const char * uart_name)
{
    int serial_fd = open(uart_name, O_RDWR | O_NOCTTY);

    if (serial_fd < 0) {
        err(1, "failed to open port: %s", uart_name);
        return false;
    }
    return serial_fd;
}


static void usage(const char *reason)
{
    if (reason) {
        fprintf(stderr, "%s\n", reason);
    }

    fprintf(stderr, "usage: obtain altitude from lightware ranger\n\n");
    exit(1);
}


int lightware_uart_main(int argc, char *argv[])
{
    if (argc < 2) {
        usage("lightware missing command");
    }

    if (!strcmp(argv[1], "start")) {
        if (thread_running) {
            warnx("lightware already running\n");
            exit(0);
        }

        thread_should_exit = false;
        daemon_task = task_spawn_cmd("lightware_uart",
                         SCHED_DEFAULT,
                         SCHED_PRIORITY_MAX - 5,
                         2000,
                         lightware_uart_thread_main,
                         (argv) ? (char * const *)&argv[2] : (char * const *)NULL);
        exit(0);
    }

    if (!strcmp(argv[1], "stop")) {
        thread_should_exit = true;
        exit(0);
    }

    if (!strcmp(argv[1], "status")) {
        if (thread_running) {
            warnx("lightware running");

        } else {
            warnx("lightware stopped");
        }

        exit(0);
    }

    usage("unrecognized comlmand");
    exit(1);
}

int lightware_uart_thread_main(int argc, char *argv[])
{
	if(argc < 2){
		errx(1, "need one serial port as input");
		usage("eg:");
	}

	const char *uart_name = argv[1];
	warnx("lightware openning port %s", uart_name);

    char data ='0' ;
    char buffer[4] = "";

    /*
     * TELEM1 : /dev/ttyS1
     * TELEM2 : /dev/ttyS2
     * GPS    : /dev/ttyS3
     * NSH    : /dev/ttyS5
     * SERIAL4: /dev/ttyS6
     * N/A    : /dev/ttyS4
     * IO DEBUG (RX only):/dev/ttyS0
     */
    int uart_read = uart_init(uart_name);
    if(false == uart_read)return -1;
    if(false == set_uart_baudrate(uart_read, atoi(argv[2]))){
        printf("lightware set_uart_baudrate is failed\n");
        return -1;
    }
    printf("lightware uart init is successful\n");

    thread_running = true;

    struct lightware_uart_data_s lightware_data;
    memset(&lightware_data, 0, sizeof(lightware_data));

    orb_advert_t lightware_uart_pub = orb_advertise(ORB_ID(lightware_uart_altitude), &lightware_data);


    while(!thread_should_exit){

        read(uart_read,&data,1);
       // printf("%c\n", data);


        if(data == '\n'){
        	for(int i = 0; i < 4;i++){
        		read(uart_read,&data,1);
        		buffer[i] = data;
        		data = '0';
        	}

        	if( buffer[0]!='.' && buffer[0] != '\n' && buffer[1]=='.'
        			&& (buffer[2] !='\n') && (buffer[3] != '\n')
        			&& (buffer[2] !='\r') && (buffer[3] != '\r')
        			&& (buffer[2] !='.') && (buffer[3] !='.'))
        	{
        		strncpy(lightware_data.datastr,buffer,4);
        		lightware_data.data = atof(lightware_data.datastr);
        		lightware_data.timestamp = hrt_absolute_time();

        		orb_publish(ORB_ID(lightware_uart_altitude), lightware_uart_pub, &lightware_data);

        		printf("%s %f\n ", buffer, (double)lightware_data.timestamp);
        	}
      }

}
    warnx("lightware exiting");
    thread_running = false;
    close(uart_read);

    fflush(stdout);

    return 0;
}
