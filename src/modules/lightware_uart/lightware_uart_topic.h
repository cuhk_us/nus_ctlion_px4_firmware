#ifndef __LIGHTWARE_UART_H_
#define __LIGHTWARE_UART_H_

#include <stdint.h>
#include <uORB/uORB.h>


ORB_DECLARE(lightware_uart_altitude);

struct lightware_uart_data_s{
    char datastr[4]; //raw data
    float data; //interpreted data
    uint64_t timestamp;
};

#endif
