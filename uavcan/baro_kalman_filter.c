#include "baro_kalman_filter.h"

static float A[3][3]; /**< transition matrix*/
static float B[3][1]; /**< input matrix*/
static float C[3];    /**< measurement matrix*/
static float D;

static float H[3][3];
static float T[3];
static float Tao_acc;
static float Tao_bias;
static float Q[3][3];
//float R[3];
static float R;
static float AA[3][3];

static float P[3][3];
static float S;
static float K[3];
static float x[3];

int baro_kalman_setX(float* kf_x) {
	for (int i=0; i<3; i++) x[i] = kf_x[i];
	return 0;
}

int baro_kalman_getX(float* kf_x){
	for (int i=0; i<3; i++) kf_x[i] = x[i];
	return 0;
}

int baro_kalman_init(void)
{
	/*setting all matrix element to be 0*/
	memset(A, 0, sizeof(A));
	memset(B, 0, sizeof(B));
	memset(C, 0, sizeof(C));
	memset(T, 0, sizeof(T));
	memset(Q, 0, sizeof(Q));

	memset(P, 0, sizeof(P));
	memset(K, 0, sizeof(K));
	memset(x, 0, sizeof(x));

	/*matrix A0*/
	A[0][1] = 1;
	A[1][2] = -1;


	/*matrix B*/
	B[1][0] = 1;


	/*matrix C0*/
	C[0] = 1;

	/*D*/
	D = 0;

	/*variance of accelerometer's gaussian noise*/
	Tao_acc = 0.3 *0.3;

	/*variance of accelerometer's bias noise*/
	//Tao_bias = 0.001 * 0.001;
	Tao_bias = 0.05 * 0.05;

	/*R*/
	R = 0.03 * 0.03 * 200 * 10;

	/*S*/
	S = 0;

	/*AB = A(i x k) * B(k x j) */

	memset(AA, 0, sizeof(AA));
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			for (int k = 0; k < 3; k++)
				AA[i][j] += A[i][k] * A[k][j];
			//printf("AA:  %8.4f\n", (float)AA[i][j]);
		}

	/*x*/
	for (int i = 0; i < 3 ; i++)
		x[i] = 0.0;

	return 0;
}



int baro_kalman_predict(float dt, float u) {

	//printf("[Baro KF Predict] u:  %8.4f\n", (double)u);

	//************************************************************************
	/** compute the discrete state space representation
	 * x = H * x + T * u
	 * where
	 * H = eye(3) + dt * A + dt^2/2 * A * A
	 * T = [dt*dt / 2;  dt]
	*/
	memset(H, 0, sizeof(H));
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			H[i][j] = dt * A[i][j] + dt * dt / 2 * AA[i][j];
			//printf("H:  %8.4f\n", (float)H[i][j]);
		}

	// compute H
	H[0][0] += (float)1.0;
	H[1][1] += (float)1.0;
	H[2][2] += (float)1.0;

	//printf("[Baro KF Predict] H: %6.3f %6.3f %6.3f\n", (double)H[0][0], (double)H[1][1], (double)H[2][2]);

	// compute T
	T[0] = dt*dt / 2;
	T[1] = dt;


	/// compute Q, process noise
	Q[0][0] = (float)pow(dt, 3) / 3 * Tao_acc + (float)pow(dt, 5) / 20 * Tao_bias;
	Q[0][1] = (float)pow(dt, 2) / 2 * Tao_acc + (float)pow(dt, 4) / 8 * Tao_bias;
	Q[0][2] = -(float)pow(dt, 3) / 6 * Tao_bias;
	Q[1][0] = (float)pow(dt, 2) / 2 * Tao_acc + (float)pow(dt, 4) / 8 * Tao_bias;
	Q[1][1] = dt * Tao_acc + (float)pow(dt, 3) / 3 * Tao_bias;
	Q[1][2] = -(float)pow(dt, 2) / 2 * Tao_bias;
	Q[2][0] = -(float)pow(dt, 3) / 6 * Tao_bias;
	Q[2][1] = -(float)pow(dt, 2) / 2 * Tao_bias;
	Q[2][2] = dt * Tao_bias;


	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++) {
			//printf("[Baro KF Predict] Q: %8.4f ", (double)Q[i][j]);
		}
		//printf("\n");
	}


	//************************************************************************
    /**
     * predict the state x
     *
     * x = H*x + T*u
     */

	// get previous state
	float x_[3];
	memset(x_, 0, sizeof(x_));
	for (int j = 0; j < 3; j++) {
		x_[j] = x[j];
		//printf("[Baro KF Predict] x_: %8.4f\n", (double)x[j]);
	}


	// get current input
	float u_ = (float) u;/**< axz */
	//printf("[Baro KF Predict] u_: %8.4f\n", (double)u_);


	// compute current state:
	// x = H*x + T*u
	float x_temp[3];
	memset(x_temp, 0, sizeof(x_temp));
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			x_temp[i] += H[i][j] * x_[j];
			//printf("H_ and x_predict_0: %8.4f %8.4f\n", (float)H[x_r][x_col], (float)x_[x_col]);
		}
		//printf("[Baro KF Predict] x_predict_1: %8.4f\n",(double)x_temp[i]);
	}

	for (int i = 0; i < 3; i++) {
		x_[i] = x_temp[i] + T[i] * u_;
		//printf("[Baro KF Predict] x_predict_2: %8.4f\n", (double)x_[i]);
	}


	//************************************************************************
	/**
	 * estimated covariance matrix P
	 *
	 * P = H * P * H.t() + Q;
	 */
	// P
	float P_[3][3];
	memset(P_, 0, sizeof(P_));
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			P_[i][j] = P[i][j];
			//printf("P_: %8.4f\n", (float)P_[i][j]);
		}

	// transpose(H)
	float H_transpose[3][3];
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			H_transpose[i][j] = H[j][i];
		}

	// H * P
	float P_temp[3][3];
	memset(P_temp, 0, sizeof(P_temp));
	for (int H_r = 0; H_r < 3; H_r++)
		for (int H_col = 0; H_col < 3; H_col++) {
			for (int P_row = 0; P_row < 3; P_row++) {
				P_temp[H_r][H_col] += H[H_r][P_row] * P_[P_row][H_col];
			}
			//printf("P_1: %8.4f\n", (float)P_temp[H_r][H_col]);
		}

	// ( H * P ) * H.t
	float P_temp_2[3][3];
	memset(P_temp_2, 0, sizeof(P_temp_2));
	for (int H_r = 0; H_r < 3; H_r++)
		for (int H_col = 0; H_col < 3; H_col++) {
			for (int P_row = 0; P_row < 3; P_row++) {
				P_temp_2[H_r][H_col] += P_temp[H_r][P_row] * H_transpose[P_row][H_col];

			}
			//printf("P_2: %8.4f\n", (float)P_temp_2[H_r][H_col]);
		}

	// ( H * P * H.t ) + Q
	for (int H_r = 0; H_r < 3; H_r++)
		for (int H_col = 0; H_col < 3; H_col++) {
			P_temp_2[H_r][H_col] += Q[H_r][H_col];
			P_[H_r][H_col] = P_temp_2[H_r][H_col];
			//printf("P_predict: %8.4f\n", (float)P_[H_r][H_col]);
		}


	//************************************************************************
	/*predict P finish*/
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			P[i][j] = P_[i][j];
			//printf("[Baro KF Predict] P: %8.4f \n", (double)P[i][j]);
		}
	}

	/*predict x finish*/
	for (int j = 0; j < 3; j++) {
		x[j] = x_[j];
		//printf("[Baro KF Predict] x: %8.4f\n", (double)x[j]);
	}

	return 0;

}


int baro_kalman_update(float dt, float z) {

	//************************************************************************
	// get x
	float x_[3];
	memset(x_, 0, sizeof(x_));

	for (size_t j = 0; j < 3; j++) {
		x_[j] = x[j];
	}

	// get P
	float P_[3][3];
	memset(P_, 0, sizeof(P_));

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			P_[i][j] = P[i][j];
			//printf("[Baro KF Update] P: %8.4f\n", (double)P_[i][j]);
		}

	//************************************************************************
	/**
	 * Update
	 * S = C * P * C.t() + R;
	 * K = P * C.t() * S.inv();
	 * x = x + K * ( z - C * x );
	 * P = P - K * C * P;
	 */

	// S
	S = P_[0][0] + R;
	//printf("S: %8.4f\n", (double)S);


	// K
	for (int K_i = 0; K_i < 3; K_i++) {
		K[K_i] = P_[K_i][0] / S;
		//printf("[Baro KF Update] K: %8.4f\n", (double)K[K_i]);
	}

	// z
	float z_ = z;
	//printf("[Baro KF Update] z_: %8.4f\n", (double)z_);


	// x = x + K * ( z - C * x );
	float x_temp[3];
	memset(x_temp, 0, sizeof(x_temp));

	for (int x_i = 0; x_i < 3; x_i++) {
		x_temp[x_i] = x_[x_i] + K[x_i] * (z_ - x_[0]);
		//printf("[Baro KF Update] x_temp: %8.4f\n", (double)x_temp[x_i]);
	}


	// P = P - K * C * P;
	float P_temp[3][3];
	memset(P_temp, 0, sizeof(P_temp));

	float P_temp_2[3][3];
	memset(P_temp_2, 0, sizeof(P_temp_2));

	float P_temp_3[3][3];
	memset(P_temp_3, 0, sizeof(P_temp_3));

	for (int P_r = 0; P_r < 3; P_r++) {
		for (int P_col = 0; P_col < 3; P_col++) {
			//printf("p_before: %8.4f %8.4f\n", (float)P_[P_r][P_col], (float)P_[P_r][P_col] * 0.1);
			P_temp[P_r][P_col] = K[P_r] * C[P_col];//P_[P_r][P_col] - P_[P_r][P_col] * 0.1;// K[P_r];
			//printf("p-after: %8.4f \n", (float)p_temp[P_r][P_col]);
		}
	}
	for (int P_r = 0; P_r < 3; P_r++) {
		for (int P_col = 0; P_col < 3; P_col++) {
			for (int P_col_2 = 0; P_col_2 < 3; P_col_2++) {
				P_temp_2[P_r][P_col] += P_temp[P_r][P_col_2] * P_[P_col_2][P_col];
			}
		}
	}

	for (int P_r = 0; P_r < 3; P_r++) {
		for (int P_col = 0; P_col < 3; P_col++) {
			P_temp_3[P_r][P_col] = P_[P_r][P_col] - P_temp_2[P_r][P_col];
		}
	}

	/*update P finish*/
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			P[i][j] = P_temp_3[i][j];
			//printf("P: %8.4f\n", (double)P[ P_r][P_col]);
		}
	}

	//************************************************************************
	/*update x finish*/
	for (int j = 0; j < 3; j++) {
		x[j] = x_temp[j];
		//printf("[Baro KF Update] x: %8.4f\n", (double)x[j]);
	}

	/*end of loop*/
	//printf("end of update\n\n\n");

	return 0;
}
